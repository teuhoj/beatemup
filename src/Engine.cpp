#include "Engine.h"

Engine::Engine()
{
    //ctor
    screenH = 600;
    screenW = 800;

    App = new sf::RenderWindow (sf::VideoMode(screenW, screenH), "Give it a Toss!");

    playhit = false;

    frames = 0;
    framesPerSecond = 0;
    oneSecond = sf::seconds(0.0f);
    FPS.setPosition(0,0);

    Player1 = 0;
    Player2 = 0;
    Enemy = 0;
    Boss = 0;

    gravity = 1200;
    airFriction = 1200;

    if(!lBuffer.loadFromFile("./audio/lightning.wav"))
    {
        std::cout << "Ei loydy salamaa" << std::endl;
    }
    else
    {
        lightning.setBuffer(lBuffer);
    }

    p1attack = false;
    p1jump = false;
    p2attack = false;
    p2jump = false;

    screenLimit = screenH / 4 * 3;

    view = sf::View (sf::Vector2f(screenW/2,screenH/2), sf::Vector2f(screenW, screenH));

//    view2p = sf::View (sf::Vector2f(screenW/2,screenH/2), sf::Vector2f(screenW, screenH));
    timeRemain = 99;

    remainingTime.setPosition(screenW/2, 0);

    leftEdge = 0;

    locking = false;
    endingLevel = false;
    levelCleared = false;

    p1runner = -1;
    p1running = false;
    p1runRight = 0;
    p2runner = -1;
    p2running = false;
    p2runRight = 0;

    Player1con = 0;
}

Engine::~Engine()
{
    //dtor

    delete App;
    App = 0;
    delete Player1;
    Player1 = 0;
    delete Player2;
    Player2 = 0;
    delete Enemy;
    Enemy = 0;
    delete Boss;
    Boss = 0;


    delete Player1con;
    Player1con = 0;

    for (unsigned int i = 0; i < Thugs.size(); ++i)
    {
        delete Thugs.at(i);
        Thugs.at(i) = 0;
    }

    for (unsigned int i = 0; i < Wontons.size(); ++i)
    {
        delete Wontons.at(i);
        Wontons.at(i) = 0;
    }

    for (unsigned int i = 0; i < Destructibles.size(); ++i)
    {
        delete Destructibles.at(i);
        Destructibles.at(i) = 0;
    }
}

void Engine::run()
{
    // Musiikin asettelua
    sf::Music ninjaMusic;
    if(!ninjaMusic.openFromFile("./audio/nga-e.ogg"))
    {
        std::cout << "Ei l�ydy nga-e.ogg" << std::endl;
        return;
    }
    ninjaMusic.setLoop(true);

    // ��net
    sf::SoundBuffer buffer;
    if(!buffer.loadFromFile("./audio/whoosh.flac"))
    {
        std::cout << "Ei l�ydy whoosh" << std::endl;
        return;
    }
    whoosh.setBuffer(buffer);
    sf::SoundBuffer lhit;
    if(!lhit.loadFromFile("./audio/light_hit.wav"))
    {
        std::cout << "Ei l�ydy light_hit" << std::endl;
        return;
    }
    light_hit.setBuffer(lhit);

    // Fontti, ainakin FPS:�� varten
    if(!viking.loadFromFile("VIKING-N.ttf"))
    {
        std::cout << "Ei l�ydy VIKING-N.ttf" << std::endl;
        return;
    }
    FPS.setFont(viking);
    remainingTime.setFont(viking);


    // Luetaan tiedostosta kent�t
    std::ifstream levelDetails ("./levels/details.dat");
    std::string line;
    while(std::getline(levelDetails,line))
    {
        levelNames.push_back(line);
    }
    levelDetails.close();


    /*
    Player1 = new BillyChing(160,100,0,5,(screenH/3)*2, 1000, screenH, screenW);
    Player2 = new BillyChing(160,100,1,15,(screenH/3)*2 + 50, 1000, screenH, screenW);
    //Enemy = new Thug(200,120,5,screenW - 150, screenH/2 + 100, 860, screenH, screenW);
    Boss = new Character(90,50,2,screenW - 50, (screenH/3)*2 - 10, 3000, screenH, screenW);*/

    /*
    for (unsigned int i = 0; i < 6; ++i)
    {
        Thug* newThug = new Thug (160, 100, i + 5, screenW - 150 + i * 150, screenH/2 + 100 - i * 5, 860, screenH, screenW);
        Thugs.push_back(newThug);
    }*/

    sf::Clock kello;

    bool start = false;

    sf::Text startGame;
    startGame.setFont(viking);
    startGame.setCharacterSize(32);
    startGame.setColor(sf::Color::Black);
    startGame.setString("Start Game");
    startGame.setPosition(screenW/2 - 50, screenH/3);
    sf::Text optionsText;
    optionsText.setFont(viking);
    optionsText.setCharacterSize(32);
    optionsText.setColor(sf::Color::Black);
    optionsText.setString("Options");
    optionsText.setPosition(screenW/2 - 50, screenH/3 + 32 + 10);
    sf::Text exitGame;
    exitGame.setFont(viking);
    exitGame.setCharacterSize(32);
    exitGame.setColor(sf::Color::Black);
    exitGame.setString("Exit Game");
    exitGame.setPosition(screenW/2 - 50, screenH/3 + 32 + 10 + 32 + 10);

    sf::ConvexShape choiceArrow;
    choiceArrow.setPointCount(7);
    choiceArrow.setPoint(0, sf::Vector2f(0,0));
    choiceArrow.setPoint(1, sf::Vector2f(50,0));
    choiceArrow.setPoint(2, sf::Vector2f(50,-10));
    choiceArrow.setPoint(3, sf::Vector2f(70,16));
    choiceArrow.setPoint(4, sf::Vector2f(50,42));
    choiceArrow.setPoint(5, sf::Vector2f(50,32));
    choiceArrow.setPoint(6, sf::Vector2f(0,32));
    choiceArrow.setFillColor(sf::Color::Blue);
    choiceArrow.setPosition(screenW/2 - 50 - 70 - 10, screenH/3);

/*
    int valinta = 0;
    float newPosY = screenH/3;
    float oldPosY = screenH/3;

    float darkness = 255;

    // Main Menun toteuts, pois k�yt�st� koska hidastaa testausta
//    mainmenu:
    while(App->isOpen() and darkness > 0)
        {
            sf::Time taken = kello.restart();

            sf::Event event;

            while (App->pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    App->close();

                if (event.type == sf::Event::KeyReleased)
                {
                    if(event.key.code == sf::Keyboard::Down)
                    {
                        valinta = valinta + 1;
                        if (valinta > 2)
                        {
                            valinta = 0;
                        }
                    }
                    if(event.key.code == sf::Keyboard::Up)
                    {
                        valinta = valinta - 1;
                        if (valinta < 0)
                        {
                            valinta = 2;
                        }
                    }
                    if(event.key.code == sf::Keyboard::Return)
                    {
                        if (valinta == 0) start = true;
                        if (valinta == 2) App->close();
                    }
                }
            }

            if(sf::Keyboard::isKeyPressed(sf::Keyboard::P))
            {
                start = true;
            }

            newPosY = screenH / 3 + valinta * (32 + 10);

            if(newPosY > oldPosY) oldPosY = oldPosY + 300 * taken.asSeconds();
            else if (newPosY < oldPosY) oldPosY = oldPosY - 300 * taken.asSeconds();
            //choiceArrow.setPosition(screenW/2 - 50 - 70 - 10,screenH / 3 + valinta * (32 + 10));

            if (oldPosY > newPosY + 2 or oldPosY < newPosY - 2)
            {
                choiceArrow.setPosition(screenW/2 - 50 - 70 - 10, oldPosY);
            }

            if (start != true)
            {
                App->clear(sf::Color::White);
            }
            else
            {
                darkness = darkness - taken.asSeconds() * 80;
                App->clear(sf::Color(darkness,darkness,darkness));
            }
            choiceArrow.setFillColor(sf::Color(0,0,255,darkness));
            App->draw(startGame);
            App->draw(optionsText);
            App->draw(exitGame);
            App->draw(choiceArrow);
            App->display();
        }
*/

//    ninjaMusic.play();

    // Hahmon valinta
    selectCharScreen();

    // KAIKKI PLAYER1 & PLAYER2 TULEE VAIHTAA VASTAAVAKSI TOIMINNALLISUUDEKSI PLAYER-LUOKAN KAUTTA
    Player1 = new BillyChing(160,100,0,5,(screenH/3)*2, 1000, screenH, screenW);
    Player2 = new BillyChing(160,100,1,15,(screenH/3)*2 + 50, 1000, screenH, screenW);
    //Enemy = new Thug(200,120,5,screenW - 150, screenH/2 + 100, 860, screenH, screenW);
    Boss = new Character(90,50,2,screenW - 50, (screenH/3)*2 - 10, 3000, screenH, screenW);


    int kentanNro = 0;
    // Luetaan kent�t tietoja
    while (App->isOpen() and kentanNro < levelNames.size())
    {
        levelCleared = false;
        locking = false;
        endingLevel = false;
        Player1->setPosition(5,400);
        Player2->setPosition(15,420);


        Player1->resetLifePos(screenW, screenH);
        Player2->resetLifePos(screenW, screenH);

        Player1con->setCharacterPosition(5,400);
        Player1con->resetLifePosition(screenH, screenW);

        view = sf::View (sf::Vector2f(screenW/2,screenH/2), sf::Vector2f(screenW, screenH));
        leftEdge = 0;

        // Resetoidaan viholliset
        while (Thugs.size() > 0)
        {
            delete Thugs.at(Thugs.size() - 1);
            Thugs.at(Thugs.size() - 1) = 0;
            Thugs.pop_back();
        }
        while (Wontons.size() > 0)
        {
            delete Wontons.at(Wontons.size() - 1);
            Wontons.at(Wontons.size() - 1) = 0;
            Wontons.pop_back();
        }
        while (Destructibles.size() > 0)
        {
            delete Destructibles.at(Destructibles.size() - 1);
            Destructibles.at(Destructibles.size() - 1) = 0;
            Destructibles.pop_back();
        }

        std::string levelFile = "./levels/";
        std::string levelPath = levelFile + levelNames.at(kentanNro);

        std::ifstream stage (levelPath.c_str());
//    std::string jana;

        std::string command;
        std::string description;

        backGround = sf::Texture();
        BACKG = sf::Sprite();

        int uusiAli = 5;

        while(stage >> command >> description)
        {
            if (command == "background")
            {
                if(!backGround.loadFromFile("./levels/bg/" + description))
                {
                    std::cout << "Ei loydy taustakuvaa kentalle " << levelNames.at(kentanNro) << std::endl;
                }
                else
                {
                    backGround.setSmooth(true);
                    BACKG.setTexture(backGround);
                    if (kentanNro == 0)
                    {
                        BACKG.setScale(screenH/916,screenH/916);
                    }
                    BACKG.setPosition(0,0);
                }
            }

            if (command == "music")
            {
                if(!bgMusic.openFromFile("./audio/" + description))
                {
                    std::cout << "Ei loydy taustamusiikkia kentalle " << levelNames.at(kentanNro) << std::endl;
                }
                else
                {
                    bgMusic.setLoop(true);
                    ninjaMusic.stop();
                    bgMusic.play();
                }
            }

        // N�in erotellaan pilkun mukaan string
        /*if (command == "enemy")
        {
            std::istringstream ss(description);
            std::string token;

            while(std::getline(ss, token, ','))
            {
                std::cout << token << std::endl;
            }
        }*/

            if (command == "event")
            {
                std::istringstream ss(description);
                std::string token;
                EVENT uusi;

            /*
            bool eka = false;
            bool toka = false;
            while (std::getline(ss, token, ','))
            {
                if (toka == true)
                {
                    uusi.yKoord = ::atof(token.c_str());
                }
                if (eka == true)
                {
                    uusi.xKoord = ::atof(token.c_str());
                    toka = true;
                }
                if (token == "THUG")
                {
                    uusi.tyyppi = SPAWNTHUG;
                    eka = true;
                }
            }*/

                std::getline(ss, token, ',');
                if (token == "THUG")
                {
                    uusi.tyyppi = SPAWNTHUG;
                }
                else if (token == "LOCK")
                {
                    uusi.tyyppi = LOCK;
                }
                else if (token == "LEVEL_END")
                {
                    uusi.tyyppi = LEVEL_END;
                }
                else if (token == "WONTON")
                {
                    uusi.tyyppi = SPAWNWONTON;
                }
                else if (token == "DESTOBJ")
                {
                    uusi.tyyppi = SPAWNDESTOBJ;
                }

                std::getline(ss, token, ',');
                uusi.xKoord = ::atof(token.c_str());
                std::getline(ss,token,',');
                uusi.yKoord = :: atof(token.c_str());

                if (uusi.tyyppi == SPAWNTHUG)
                {
                    Thug* uusiThug = new Thug(160,50, uusiAli , uusi.xKoord, uusi.yKoord, 860, screenH, screenW);
                    Thugs.push_back(uusiThug);

                    uusiAli = uusiAli + 1;
                }

                else if (uusi.tyyppi == SPAWNDESTOBJ)
                {
                    std::getline(ss, token, ',');
                    if (token == "WONTON")
                    {
                        uusi.contains = WONTON;
                    }
                    std::getline(ss, token, ',');
                    if (token == "TRASHCAN")
                    {
                        uusi.appearance = TRASHCAN;
                    }
                }

                tapahtumat.push_back(uusi);
            }
        }

        std::cout << tapahtumat.size() << std::endl;

    /*while(std::getline(stage,jana))
    {
        std::string sana = "";
        int muisti = 0;
        for(unsigned int i = 0; i < jana.size(); ++i)
        {
            char testi = jana.at(i);
            if(testi == ' ')
            {
                muisti = i + 1;
                break;
            }
            sana = sana + jana.at(i);
        }
        std::string tKuvaPath;
        if (sana == "background")
        {
            for(unsigned int i = muisti; i < jana.size(); ++i)
            {
                tKuvaPath = tKuvaPath + jana.at(i);
            }

            std::string path = "./levels/bg/" + tKuvaPath;
            if(!backGround.loadFromFile(path))
            {
                std::cout << "Ei loydy kuvaa" << std::endl;
            }
            backGround.setSmooth(true);
            BACKG.setTexture(backGround);
            BACKG.scale(screenH/916,screenH/916);
            BACKG.setPosition(0,0);
        }
    }*/
        stage.close();

        while (App->isOpen() and levelCleared == false)
        {
            sf::Event event;

            while (App->pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    App->close();

                if (event.type == sf::Event::KeyReleased)
                {
                    if(event.key.code == sf::Keyboard::Numpad8)
                    {
                        p1jump = false;
                    }
                    if(event.key.code == sf::Keyboard::Numpad7)
                    {
                        p1attack = false;
                    }
                    if(event.key.code == sf::Keyboard::Y)
                    {
                        p2jump = false;
                    }
                    if(event.key.code == sf::Keyboard::T)
                    {
                        p2attack = false;
                    }

                    // Juoksun yrityst�
                    if (event.key.code == sf::Keyboard::Right or event.key.code == sf::Keyboard::Left)
                    {
                        if (p1running == true)
                        {
                            p1runner = -1;
                            p1running = false;
                            p1runRight = 0;
                        }
                        else
                        {
                            p1runner = 0;
                            if (event.key.code == sf::Keyboard::Right)
                            {
                                p1runRight = 1;
                            }
                            else
                            {
                                p1runRight = 2;
                            }
                        }
                    }
                    if (event.key.code == sf::Keyboard::A or event.key.code == sf::Keyboard::D)
                    {
                        if (p2running == true)
                        {
                            p2runner = -1;
                            p2running = false;
                            p2runRight = 0;
                        }
                        else
                        {
                            p2runner = 0;
                            if(event.key.code == sf::Keyboard::D)
                            {
                                p2runRight = 1;
                            }
                            else
                            {
                                p2runRight = 2;
                            }
                        }
                    }
                }
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                App->close();
//            start = false;
//            darkness = 255;
//            ninjaMusic.stop();
//            goto mainmenu;
            }

            sf::Time taken = kello.restart();


            float rightEdge = leftEdge + screenW;



            // Juoksun aikariippuvuus
            if (p1runner >= 0 and p1runner <= 0.6)
            {
                p1runner = p1runner + taken.asSeconds();
            }
            if (p2runner >= 0 and p2runner <= 0.6)
            {
                p2runner = p2runner + taken.asSeconds();
            }



            // Katsotaan, tuleeko tapahtuma toimeen
            for (unsigned int i = 0; i < tapahtumat.size(); ++i)
            {
                if (tapahtumat.at(i).xKoord <= rightEdge + 50)
                {
                    if(tapahtumat.at(i).tyyppi == SPAWNTHUG)
                    {
                        /*
                        int uusiAli = 5;
                        if (Thugs.size() > 0)
                        {
                            uusiAli = Thugs.at(Thugs.size() - 1)->getType() + 1;
                        }
/*                        Thug* uusi = new Thug(160,50, uusiAli , tapahtumat.at(i).xKoord, tapahtumat.at(i).yKoord, 860, screenH, screenW);
                        Thugs.push_back(uusi);*/

//                        std::cout << "new thug here " << uusi->getType() << std::endl;
                        for (unsigned int a = 0; a < Thugs.size(); ++a)
                        {
                            if (tapahtumat.at(i).xKoord == Thugs.at(a)->getPosX() and tapahtumat.at(i).yKoord == Thugs.at(a)->getPosY())
                            {
                                Thugs.at(a)->setActive();
                                std::cout << "Thug activated" << std::endl;
                            }
                        }
                    }
                    else if (tapahtumat.at(i).tyyppi == LOCK)
                    {
                        locking = true;
                        std::cout << "Eliminate all enemies!" << std::endl;
                    }
                    else if (tapahtumat.at(i).tyyppi == LEVEL_END)
                    {
                        locking = true;
                        endingLevel = true;
                        std::cout << "Dispatch all enemies to end stage" << std::endl;
                    }

                    else if (tapahtumat.at(i).tyyppi == SPAWNWONTON)
                    {
                        Wonton* uusi = new Wonton(tapahtumat.at(i).xKoord, tapahtumat.at(i).yKoord, uusiAli);
                        Wontons.push_back(uusi);

                        uusiAli = uusiAli + 1;
                    }

                    else if (tapahtumat.at(i).tyyppi == SPAWNDESTOBJ)
                    {
                        Destructible* uusi = new Destructible(tapahtumat.at(i).xKoord, tapahtumat.at(i).yKoord, uusiAli, tapahtumat.at(i).appearance, tapahtumat.at(i).contains);
                        Destructibles.push_back(uusi);

                        uusiAli = uusiAli + 1;
                    }

                    EVENT korvaava = tapahtumat.at(tapahtumat.size() - 1);
                    tapahtumat.at(tapahtumat.size() - 1) = tapahtumat.at(i);
                    tapahtumat.at(i) = korvaava;
                    tapahtumat.pop_back();
                }
            }


            // Resetoidaan kaikki agentit
            Player1->resetCharacter();
            Player2->resetCharacter();

            Player1con->resetCharacter();

            for (unsigned int i = 0; i < Thugs.size(); ++i)
            {
                Thugs.at(i)->resetCharacter();
            }


            bool p1Liikkuu = false;
            bool p2Liikkuu = false;

            // Pelaajan 1 kontrollit
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            {
//                Player1->moveCharacter(YLOS, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                Player1con->moveCharacter(YLOS, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);

                p1Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            {
  //              Player1->moveCharacter(ALAS, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                Player1con->moveCharacter(ALAS, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);

                p1Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            {
                if (((p1runner >= 0 and p1runner <= 0.6) or p1running == true) and p1runRight == 1)
                {
//                    Player1->moveCharacter(OIKEA, taken.asSeconds()*400, screenLimit, screenH, leftEdge, rightEdge, true);
                    Player1con->moveCharacter(OIKEA, taken.asSeconds()*400, screenLimit, screenH, leftEdge, rightEdge, true);

                    p1running = true;
                }
                else
                {
  //                  Player1->moveCharacter(OIKEA, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                    Player1con->moveCharacter(OIKEA, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                }
                p1Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            {
                if (((p1runner >= 0 and p1runner <= 0.6) or p1running == true) and p1runRight == 2)
                {
//                    Player1->moveCharacter(VASEN, taken.asSeconds()*400, screenLimit, screenH, leftEdge, rightEdge, true);
                    Player1con->moveCharacter(VASEN, taken.asSeconds()*400, screenLimit, screenH, leftEdge, rightEdge, true);
                    p1running = true;
                }
                else
                {
  //                  Player1->moveCharacter(VASEN, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                    Player1con->moveCharacter(VASEN, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                }
                p1Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8) and p1jump == false)
            {
                p1jump = true;
//                Player1->jumpCharacter(800);
                Player1con->jumpCharacter(800);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7) and p1attack == false)
            {
                p1attack = true;
                bool pickingUp = false;

                for (unsigned int i = 0; i < Wontons.size(); ++i)
                {
                    // LAITETAAN MIELUUMMIN TESTI CHARACTERIN FUNKTIOLLE!!!
                    // HUOM!!! KORVAA PLAYER -FUNKTIOLLA
                    if (Player1con->checkItemProximity(Wontons.at(i)) == true)
                    //if (Player1->checkItemProximity(Wontons.at(i)) == true)
                    {
                        delete Wontons.at(i);
                        Wontons.at(i) = Wontons.at(Wontons.size() - 1);
                        Wontons.at(Wontons.size() - 1) = 0;
                        Wontons.pop_back();

                        pickingUp = true;
                        break;
                    }
                }

                if (pickingUp == false)
                {
//                    Player1->attack();
                    Player1con->attackCharacter();
                }
            }
            // Kiinnipitotestit vain, jos hahmo liikkuu eik� ly� tai hypp��
            // T�SS�KIN KORVAA N�M� Player1 --> Player1con
            if (p1Liikkuu == true and Player1con->isCharacterAttacking() == false and Player1con->isCharacterJumping() == false)
            //if (p1Liikkuu == true and Player1->isAttacking() == false and Player1->isJumping() == false)
            {
                // Tarkistetaan vihollisten sijainti pelihahmoon n�hden
                for (unsigned int i = 0; i < Thugs.size(); ++i)
                {
                    if (Thugs.at(i)->getActive() == true)
                    {
//                        Player1->checkEnemyProximity(Thugs.at(i));
                        Player1con->checkEnemyProximity(Thugs.at(i));
                    }
                }
            }



            // Pelaajan 2 kontrollit
            // Korvaa Playert2 Playert2con:lla
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            {
                Player2->moveCharacter(YLOS, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                p2Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            {
                Player2->moveCharacter(ALAS, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                p2Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
            {
                if (((p2runner >= 0 and p2runner <= 0.6) or p2running == true) and p2runRight == 1)
                {
                    Player2->moveCharacter(OIKEA, taken.asSeconds()*400, screenLimit, screenH, leftEdge, rightEdge, true);
                    p2running = true;
                }
                else
                {
                    Player2->moveCharacter(OIKEA, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                }
                p2Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
            {
                if (((p2runner >= 0 and p2runner <= 0.6) or p2running == true) and p2runRight == 2)
                {
                    Player2->moveCharacter(VASEN, taken.asSeconds()*400, screenLimit, screenH, leftEdge, rightEdge, true);
                    p2running = true;
                }
                else
                {
                    Player2->moveCharacter(VASEN, taken.asSeconds()*200, screenLimit, screenH, leftEdge, rightEdge, false);
                }
                p2Liikkuu = true;
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::Y) and p2jump == false)
            {
                p2jump = true;
                Player2->jumpCharacter(500);
            }
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::T) and p2attack == false)
            {
                //p2attack = true;
                //Player2->attack();

                p2attack = true;
                bool pickingUp = false;

                for (unsigned int i = 0; i < Wontons.size(); ++i)
                {
                    // LAITETAAN MIELUUMMIN TESTI CHARACTERIN FUNKTIOLLE!!!
                    if (Player2->checkItemProximity(Wontons.at(i)) == true)
                    {
                        delete Wontons.at(i);
                        Wontons.at(i) = Wontons.at(Wontons.size() - 1);
                        Wontons.at(Wontons.size() - 1) = 0;
                        Wontons.pop_back();

                        pickingUp = true;
                        break;
                    }
                }

                if (pickingUp == false)
                {
                    Player2->attack();
                }
            }

            // Kiinnipitotestit toiselle pelaajalle
            if (p2Liikkuu == true and Player2->isAttacking() == false and Player2->isJumping() == false)
            {
                // Tarkistetaan vihollisten sijainti pelihahmoon n�hden
                for (unsigned int i = 0; i < Thugs.size(); ++i)
                {
                    if (Thugs.at(i)->getActive() == true)
                    {
                        Player2->checkEnemyProximity(Thugs.at(i));
                    }
                }
            }

        /*
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::G))
        {
            Enemy->jumpCharacter(500);
        }*/
            if(sf::Keyboard::isKeyPressed(sf::Keyboard::H))
            {
                Boss->jumpCharacter(500);
            }



        // Testi� ja hauskuutusta
            /*if(sf::Keyboard::isKeyPressed(sf::Keyboard::L))
            {
                if(lightning.getStatus()!= sf::Sound::Playing)
                {
                    lightning.play();
                }
            }*/



            // V�h�n FPS laskuja
            frames = frames + 1;
            oneSecond = oneSecond + taken;
            if (oneSecond.asSeconds() >= 1.0f)
            {
                framesPerSecond = frames;
                frames = 0;
                oneSecond = sf::seconds(0.0f);

                timeRemain = timeRemain - 1;
            }


            // Toimintaosuus!!!

            // Vihollisten teko�ly
        /*if (Enemy != 0)
        {
            Enemy->act(screenLimit, screenH, leftEdge, rightEdge, taken.asSeconds(), Player1->getPosY() + Player1->getHeight(), Player1->getPosX());
        }*/

            for (unsigned int i = 0; i < Thugs.size(); ++i)
            {
//                Thugs.at(i)->act(screenLimit, screenH, leftEdge, rightEdge, taken.asSeconds(), Player1->getPosY() + Player1->getHeight(), Player1->getPosX());
                Thugs.at(i)->act(screenLimit, screenH, leftEdge, rightEdge, taken.asSeconds(), Player1con->getPosY() + Player1con->getHeight(), Player1con->getPosX());
            }

            std::vector <DANGER> dangerAreas;

            DANGER current;
//            if(Player1->dangerZone(current.zone, current.damage, current.stagePosY, current.healthPos) == true)
  //          {
    //            dangerAreas.push_back(current);
      //      }
            if (Player1con->dangerZone(current.zone, current.damage, current.stagePosY, current.healthPos) == true)
            {
                dangerAreas.push_back(current);
            }

            if(Player2->dangerZone(current.zone, current.damage, current.stagePosY, current.healthPos) == true)
            {
                dangerAreas.push_back(current);
            }

//            Player1->updateCharacter(taken.asSeconds()*gravity, taken.asSeconds(), taken.asSeconds()*airFriction);
            Player1con->updateCharacter(taken.asSeconds()*gravity, taken.asSeconds(), taken.asSeconds()*airFriction);

            Player2->updateCharacter(taken.asSeconds()*gravity, taken.asSeconds(), taken.asSeconds()*airFriction);
        /*if (Enemy != 0)
        {
            Enemy->updateCharacter(taken.asSeconds()*gravity, taken.asSeconds());
        }*/


            for (unsigned int i = 0; i < Thugs.size(); ++i)
            {
                Thugs.at(i)->updateCharacter(taken.asSeconds()*gravity, taken.asSeconds(), taken.asSeconds()*airFriction);
            }

            Boss->updateCharacter(taken.asSeconds()*gravity, taken.asSeconds(), taken.asSeconds()*airFriction);

            // Muista p�ivitt�� tavaratkin!!!
            for (unsigned int i = 0; i < Wontons.size(); ++i)
            {
                Wontons.at(i)->updateObject(taken.asSeconds() * gravity, taken.asSeconds());
            }

//            Player1->updatePunching(taken.asSeconds()*gravity, taken.asSeconds());
            Player1con->updatePunching(taken.asSeconds()*gravity, taken.asSeconds());

            Player2->updatePunching(taken.asSeconds()*gravity, taken.asSeconds());
        /*if (Enemy != 0)
        {
            Enemy->updatePunching(taken.asSeconds()*gravity, taken.asSeconds());
        }*/


            for (unsigned int i = 0; i < Thugs.size(); ++i)
            {
                Thugs.at(i)->updatePunching(taken.asSeconds()*gravity, taken.asSeconds());
            }

            Boss->updatePunching(taken.asSeconds()*gravity, taken.asSeconds());

            for (unsigned int i = 0; i < dangerAreas.size(); ++i)
            {
            /*
            if (Enemy != 0)
            {
                Enemy->takeHit(dangerAreas.at(i).zone, dangerAreas.at(i).damage, dangerAreas.at(i).stagePosY);
            }*/

                for (unsigned int a = 0; a < Thugs.size(); ++a)
                {
                    Thugs.at(a)->takeHit (dangerAreas.at(i).zone, dangerAreas.at(i).damage, dangerAreas.at(i).stagePosY, dangerAreas.at(i).healthPos);
                }
                for (unsigned int a = 0; a < Destructibles.size(); ++a)
                {
                    Destructibles.at(a)->takeDamage(dangerAreas.at(i).zone, dangerAreas.at(i).damage, dangerAreas.at(i).stagePosY);
                }
            }

        /*
        if(Enemy != 0 and Enemy->checkDeath() == true)
        {
            delete Enemy;
            Enemy = 0;
        }*/

            // Poistellaan ylimaaraiset viholliset ja rikkoutuneet esineet
            for (unsigned int i = 0; i < Thugs.size(); ++i)
            {
                if (Thugs.at(i)->checkDeath() == true)
                {
                    Thug* testi = Thugs.at(Thugs.size() - 1);
                    Thugs.at(Thugs.size() - 1) = Thugs.at(i);
                    Thugs.at(i) = testi;
                    delete Thugs.at(Thugs.size() - 1);
                    Thugs.at(Thugs.size() - 1) = 0;
                    Thugs.pop_back();
                }
            }
            //std::cout << Wontons.size() << std::endl;

            for (unsigned int i = 0; i < Destructibles.size(); ++i)
            {
                if (Destructibles.at(i)->getHealth() <= 0)
                {
                    Destructible* tuhottu = Destructibles.at(i);
                    Destructible* viimeinen = Destructibles.at(Destructibles.size() - 1);
                    Destructibles.at(Destructibles.size() - 1) = Destructibles.at(i);
                    Destructibles.at(i) = viimeinen;

                    if (tuhottu->getContents() == WONTON)
                    {
                        Wonton* uusi = new Wonton(0,0,uusiAli);
                        uusiAli = uusiAli + 1;

                        uusi->setPosition(tuhottu->getPosX() + tuhottu->getWidth()/2 - uusi->getWidth()/2, tuhottu->getPosY() + tuhottu->getHeight() - uusi->getHeight());
                        uusi->jumpObject(500);

                        Wontons.push_back(uusi);
                        //std::cout << "hepo" << std::endl;
                    }

                    delete Destructibles.at(Destructibles.size() - 1);
                    Destructibles.at(Destructibles.size() - 1) = 0;
                    Destructibles.pop_back();
                }
            }

            // Muokataan health-baareja MUISTA MY�S PELAAJAHAHMOILLE
            /*for (unsigned int i = 0; i < Thugs.size(); ++i)
            {
                Thugs.at(i)->calculateLife(screenH);
            }*/

            /*
            if (Thugs.size() == 0 and locking == true)
            {
                locking = false;
                if (endingLevel == true)
                {
                // TILAP�INEN KENT�N P��TT�MINEN
//                App->close();
                    levelCleared = true;
                }
            }*/

            // Tarkistetaan, tarvitseeko kentta lukita vihollisten voittamisen ajaksi
            if (locking == true)
            {
                int activatedEnemies = 0;
                for (unsigned int i = 0; i < Thugs.size(); ++i)
                {
                    if (Thugs.at(i)->getActive() == true)
                    {
                        activatedEnemies = activatedEnemies + 1;
                    }
                }
                if (activatedEnemies <= 0)
                {
                    locking = false;
                    if (endingLevel == true)
                    {
                        levelCleared = true;
                    }
                }
            }

            updateGame(dangerAreas, taken);
        }

//    ninjaMusic.stop();
        bgMusic.stop();

        kentanNro = kentanNro + 1;
        if (kentanNro >= levelNames.size())
        {
            // MUISTA VAIHTAA SEN SIJAAN LOPPUKUVIEN N�YTT�MISEEN
            App->close();
        }
    }
}

void Engine::updateGame(std::vector<DANGER> dangerAreas, sf::Time theClock)
{
    sf::RectangleShape limit(sf::Vector2f(screenW, screenH / 4));
    limit.setFillColor(sf::Color(131,150,156,127));
    limit.setPosition(0.0f,screenLimit);

    //sf::CircleShape shape(100.f);
    //shape.setFillColor(sf::Color::Green);

    // T�m� muodostetaan FPS:t
    std::stringstream ss;
    ss << framesPerSecond;
    FPS.setString(ss.str());
    FPS.setCharacterSize(16);
    FPS.setColor(sf::Color::White);

    /*
    std::stringstream st;
    st << timeRemain;
    remainingTime.setString(st.str());
    remainingTime.setCharacterSize(20);
    remainingTime.setColor(sf::Color::White);
    */
    /*
    sf::RectangleShape rectangle(sf::Vector2f(120,50));
    rectangle.setFillColor(sf::Color::Red);

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::M))
        {
            rectangle.setPosition(120.0f, 260.0f);
            if(whoosh.getStatus()!= sf::Sound::Playing)
            {
                whoosh.play();
                playhit = true;
                //light_hit.play();
            }
        }

    if (playhit == true and whoosh.getStatus()==sf::Sound::Stopped)
    {
        light_hit.play();
        playhit = false;
    }*/

//    view.setCenter(Player1->getPosX(), Player1->getPosY());
//    player1View.setViewport(sf::FloatRect(0, 0, 0.5f, 1));
//    view.setViewport(sf::FloatRect(0,0,0.5f,1));

    float movex = 0;
    // Korvataan Player1 ja Player2 Player1con:lla sek� Player2con:lla
    //Player1con->getPosX();
//    if(locking == false and Player1->getPosX() > view.getCenter().x + screenW/2 - 300 and Player2->getPosX() > view.getCenter().x + screenW/2 - 300)
    if(locking == false and Player1con->getPosX() > view.getCenter().x + screenW/2 - 300 and Player2->getPosX() > view.getCenter().x + screenW/2 - 300)
    {
        movex = theClock.asSeconds() * 200;
        view.move(movex, 0);
        FPS.move(movex, 0);
        leftEdge = leftEdge + movex;
//        remainingTime.move(movex, 0);
    }

    App->setView(view);

    App->clear(sf::Color::White);
    App->draw(BACKG);
    App->draw(limit);

    for(unsigned int i = 0; i < dangerAreas.size(); ++i)
    {
        App->draw(dangerAreas.at(i).zone);
    }

//    App->draw(rectangle);


    std::vector <Character*> lista;
//    lista.push_back(Player1);
    lista.push_back(Player2);


    lista.push_back(Player1con->returnCurrentCharacter());


    /*if (Enemy != 0)
    {
        lista.push_back(Enemy);
    }*/

    for (unsigned int i = 0; i < Thugs.size(); ++i)
    {
        lista.push_back(Thugs.at(i));
    }

    lista.push_back(Boss);

    std::vector <Object*> tavarat;
    for (unsigned int i = 0; i < Wontons.size(); ++i)
    {
        tavarat.push_back(Wontons.at(i));
    }
    for (unsigned int i = 0; i < Destructibles.size(); ++i)
    {
        tavarat.push_back(Destructibles.at(i));
    }

    std::vector <int> jarjestys;

    // Piirtoj�rjestys y-akselin sijainnin mukaan. L�hin piiret��n viimeisen�
//    for (unsigned int i = 0; i < lista.size(); ++i)



    while (lista.size() > 0 or tavarat.size() > 0)
    {

        Character* lahin = 0;
        Object* lahinTavara = 0;
        // Henkil�t
        if (lista.size() > 0)
        {
            int loppu = lista.size() - 1;
            /*Character* */lahin = lista.at(loppu);
            for (int i = lista.size() - 2 ; i >= 0; --i)
            {

                if (lahin->getPosY() + lahin->getHeight() < lista.at(i)->getPosY() + lista.at(i)->getHeight())
                {

                    lista.at(loppu) = lista.at(i);
                    lista.at(i) = lahin;
                    lahin = lista.at(loppu);
                }

            }
        }


        // Tavarat
        if (tavarat.size() > 0)
        {
            int tavaraLoppu = tavarat.size() - 1;
            /*Object* */lahinTavara = tavarat.at(tavaraLoppu);
            for (int i = tavarat.size() - 2; i >= 0; --i)
            {
                if (lahinTavara->getPosY() + lahinTavara->getHeight() < tavarat.at(i)->getPosY() + tavarat.at(i)->getHeight())
                {
                    tavarat.at(tavaraLoppu) = tavarat.at(i);
                    tavarat.at(i) = lahinTavara;
                    lahinTavara = tavarat.at(tavaraLoppu);
                }
            }
        }

        if (lahin == 0)
        {
            jarjestys.push_back(lahinTavara->getType());
            tavarat.pop_back();
        }
        else if (lahinTavara == 0)
        {
            jarjestys.push_back(lahin->getType());
            lista.pop_back();
        }
        else if (lahin->getPosY() + lahin->getHeight() < lahinTavara->getPosY() + lahinTavara->getHeight())
        {
            jarjestys.push_back(lahinTavara->getType());
            tavarat.pop_back();
        }
        else
        {
            jarjestys.push_back(lahin->getType());
            lista.pop_back();
        }
    }


    // Lopuksi piirret��n kaikki objektit l�heisyytens� mukaan
    for (int i = jarjestys.size() - 1; i >= 0; --i)
    {
        if (jarjestys.at(i) == 0)
        {
//            Player1->drawCharacter(App, theClock);

            Player1con->drawCurrentCharacter(App, theClock);
        }
        else if (jarjestys.at(i) == 1)
        {
            Player2->drawCharacter(App, theClock);
        }
        else if (jarjestys.at(i) == 2)
        {
            Boss->drawCharacter(App, theClock);
        }
        /*
        else if (jarjestys.at(i) == 5)
        {
            Enemy->drawCharacter(App, theClock);
        }*/
        else
        {
            for (unsigned int a = 0; a < Thugs.size(); ++a)
            {
                if (jarjestys.at(i) == Thugs.at(a)->getType())
                {
                    Thugs.at(a)->drawCharacter(App, theClock);
                }
            }
            for (unsigned int b = 0; b < Wontons.size(); ++b)
            {
                if (jarjestys.at(i) == Wontons.at(b)->getType())
                {
                    Wontons.at(b)->drawObject(App);
                }
            }
            for (unsigned int c = 0; c < Destructibles.size(); ++c)
            {
                if (jarjestys.at(i) == Destructibles.at(c)->getType())
                {
                    Destructibles.at(c)->drawObject(App);
                }
            }
        }
    }

    /*for (unsigned int i = 0; i < Destructibles.size(); ++i)
    {
        Destructibles.at(i)->drawObject(App);
    }*/

    // MUISTA ASETTAA MY�S PIIRTOJARJESTYS
    /*for (unsigned int i = 0; i < Wontons.size(); ++i)
    {
        Wontons.at(i)->drawObject(App);
    }*/

    /*
    for (unsigned int i = 0; i < Thugs.size(); ++i)
    {
        Thugs.at(i)->drawCharacter(App, theClock);
    }*/

    /*
    // Piirret��n pelihahmot
    Player1->drawCharacter(App, theClock);
    Player2->drawCharacter(App, theClock);

    if (Enemy != 0)
    {
        Enemy->drawCharacter(App, theClock);
    }

    Boss->drawCharacter(App, theClock);

    */

    // Piirret��n tarpeelliset kestot
//    Player1->drawLife(App, movex, screenH);
    Player2->drawLife(App, movex, screenH);

    Player1con->drawLife(App, movex, screenH);

    for (unsigned int i = 0; i < Thugs.size(); ++i)
    {
        Thugs.at(i)->drawLife(App, movex, screenH);
    }



    App->draw(FPS);
//    App->draw(remainingTime);


    // TESTI�!!! 2-PLAYER SPLIT SCREEN
    /*
    view2p.setCenter(Player2->getPosX(), Player2->getPosY());
    view2p.setViewport(sf::FloatRect(0.5f, 0, 0.5f, 1));
    App->setView(view2p);
    App->draw(BACKG);
    App->draw(limit);

    for(unsigned int i = 0; i < dangerAreas.size(); ++i)
    {
        App->draw(dangerAreas.at(i).zone);
    }

//    App->draw(rectangle);
    // Piirret��n pelihahmot
    Player1->drawCharacter(App, theClock);
    Player2->drawCharacter(App, theClock);
    Enemy->drawCharacter(App, theClock);
    Boss->drawCharacter(App, theClock);

    // Piirret��n tarpeelliset kestot
    Player1->drawLife(App);
    Player2->drawLife(App);

    App->draw(FPS);

    */


    App->display();
}

// Hahmonvalintaruudut
void Engine::selectCharScreen()
{
    Player1con = new Player();

    bool valintaTehty = false;
    sf::Clock valintaKello;

    sf::RectangleShape valintaRuutu1 = sf::RectangleShape(sf::Vector2f(50, 50));
    valintaRuutu1.setFillColor(sf::Color::White);
    valintaRuutu1.setOutlineThickness(5);
    valintaRuutu1.setOutlineColor(sf::Color::Red);
    valintaRuutu1.setOrigin(25,25);
    valintaRuutu1.setPosition(screenW/2, screenH/2);

    int valinta1 = 0;


    sf::Text charName;
    charName.setFont(viking);
    charName.setCharacterSize(24);
    charName.setColor(sf::Color::White);
    charName.setPosition(0,0);


    while (App->isOpen() and valintaTehty == false)
    {
        sf::Event event;

        while (App->pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                App->close();
            }
            else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                App->close();
            }
            else if (event.type == sf::Event::KeyReleased)
            {
                if(event.key.code == sf::Keyboard::Up)
                {
                    valinta1 = valinta1 - 2;
                }
                else if(event.key.code == sf::Keyboard::Down)
                {
                    valinta1 = valinta1 + 2;
                }
                else if (event.key.code == sf::Keyboard::Right)
                {
                    valinta1 = valinta1 + 1;
                }
                else if (event.key.code == sf::Keyboard::Left)
                {
                    valinta1 = valinta1 - 1;
                }
            }
        }
        //std::cout << valinta1 << std::endl;

        //sf::Time valintaAika = valintaKello.restart();

        if (valinta1 >= 4)
        {
            valinta1 = valinta1 - 4;
        }
        if (valinta1 < 0)
        {
            valinta1 = valinta1 + 4;
        }

        float xSuunta = 0;
        if (valinta1 == 0 or valinta1 == 2)
        {
            xSuunta = -25;
        }
        else if (valinta1 == 1 or valinta1 == 3)
        {
            xSuunta = 25;
        }
        float ySuunta = 0;
        if (valinta1 == 0 or valinta1 == 1)
        {
            ySuunta = -25;
        }
        else if (valinta1 == 2 or valinta1 == 3)
        {
            ySuunta = 25;
        }
        valintaRuutu1.setPosition(screenW/2 + xSuunta, screenH/2 + ySuunta);


        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Return))
        {
            if (valinta1 == 0)
            {
                Player1con->selectBilly(screenH, screenW, 0);
                valintaTehty = true;
            }
            else if (valinta1 == 2)
            {
                Player1con->selectKaiser(screenH, screenW, 0);
                valintaTehty = true;
            }
            else
            {
                std::cout << "Coming soon!" << std::endl;
            }
        }

        App->clear(sf::Color::Black);



        if (valinta1 == 0)
        {
            charName.setString("Billy Ching");
        }
        else if (valinta1 == 1)
        {
            charName.setString("Sammy Chong");
        }
        else if (valinta1 == 2)
        {
            charName.setString("Kaiser Chef");
        }
        else if (valinta1 == 3)
        {
            charName.setString("Nippon Ninja");
        }

        App->draw(charName);

        App->draw(valintaRuutu1);

        App->display();
    }
}
