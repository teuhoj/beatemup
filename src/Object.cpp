#include "Object.h"

Object::Object(float xKoord, float yKoord, int jarj)
{
    //ctor
    posX = xKoord;
    posY = yKoord;

    hitbox.setPosition(posX, posY);

    ali = jarj;

    jumping = false;
    airSpeed = 0;
    airPos = 0;
}

Object::~Object()
{
    //dtor
}

void Object::drawObject(sf::RenderWindow* Window)
{
    Window->draw(shadow);
    Window->draw(hitbox);
}

float Object::getHealing()
{
    return amountHealed;
}

int Object::getType()
{
    return ali;
}

float Object::getPosX()
{
    return posX;
}
float Object::getPosY()
{
    return posY;
}
float Object::getHeight()
{
    return height;
}
float Object::getWidth()
{
    return width;
}

void Object::setPosition(float positX, float positY)
{
    posX = positX;
    posY = positY;
    hitbox.setPosition(posX, posY);
}

void Object::jumpObject(float amount)
{
    if (jumping == false)
    {
        jumping = true;
        airSpeed = amount;
    }
}

void Object::updateObject(float gravity, float timeStuff)
{
    if (jumping == true)
    {
        // Lasketaan liikenopeutta
        airSpeed = airSpeed - gravity;
        // Muokataan ilmasijaintia nopeuden mukaisesti
        airPos = airPos + airSpeed * timeStuff;
        // Pienennetaan varjoa hypyn korkeuteen nahden
        // Jos ollaan yli maksimaalisen rajan, ei pienenneta varjoa enempaa
        float maksimaalista = airPos/20;
        if(width/2 - maksimaalista >= 0)
        {
            shadow.setRadius(width/2 - maksimaalista);
        }

        shadow.setPosition(posX - width/4 + airPos/15,posY + height - width/4);

//        flip = flip + 410 * timeStuff;
//        std::cout << punch.getPosition().x << " " << punch.getPosition().y << std::endl;
    }

    if (posY - airPos > posY)
    {
        //flip = 0;

        jumping = false;
        airPos = 0;
        airSpeed = 0;
        shadow.setRadius(width/2);
        shadow.setPosition(posX - width/4,posY + height - width/4);
//        shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
    }
}
