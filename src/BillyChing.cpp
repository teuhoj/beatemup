#include "BillyChing.h"

BillyChing::BillyChing(int align, float setX, float setY, float hp, float screenH, float screenW) : Character(align, setX, setY, hp, screenH, screenW)
{
    //ctor
    std::string path = "./sprites/BillyC/BC_Stance_";
    std::string suffix = ".png";

    standFrames = 8;


    /*
    for(unsigned int i = 0; i < 5; ++i)
    {
        sf::Texture test;
        if(!test.loadFromFile("./sprites/BillyC/BCStance0.png"))
        {
            std::cout << "Ttesti" << std::endl;
        }
        standingText.push_back(test);
    }*/




    for(unsigned int a = 0; a < standFrames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture standTex;
        if(!standTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCStance.png" << std::endl;
        }
        standingText.push_back(standTex);
    }


//    standFrames = standFrames + 5;


    // Asetetaan spritet
    for(unsigned int i = 0; i < standingText.size(); ++i)
    {
        standingText.at(i).setSmooth(true);

        sf::Sprite stand;
        stand.setOrigin(250,250);
        stand.setTexture(standingText.at(i));
        stand.setScale(0.4,0.4);
        standingSprite.push_back(stand);
    }

    // Asetetaan tekstuurit ly�nnille 1
    path = "./sprites/BillyC/BC_Punch";
    punch1Frames = 5;

    for (int a = 0; a < punch1Frames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture punchTex;
        if(!punchTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCPunch.png" << std::endl;
        }
//        punchTex.setSmooth(true);
        punch1Text.push_back(punchTex);

        /*
        sf::Sprite punch;
        punch.setOrigin(250,250);
        punch.setTexture(punch1Text.at(a));
        punch.setScale(0.4,0.4);
        punch1Sprite.push_back(punch);*/
    }

    for (unsigned int i = 0; i < punch1Text.size(); ++i)
    {
        punch1Text.at(i).setSmooth(true);
        sf::Sprite punch;
        punch.setOrigin(250,250);
        punch.setTexture(punch1Text.at(i));
        punch.setScale(0.4,0.4);
        punch1Sprite.push_back(punch);
    }

    // Textuurit k�velylle
    path = "./sprites/BillyC/BC_Walk_";
    walkFrames = 8;
    for (int i = 0; i < walkFrames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture walk;
        if (!walk.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCWalk" << i << ".png" << std::endl;
        }
        walkText.push_back(walk);
    }
    for (unsigned int i = 0; i < walkText.size(); ++i)
    {
        walkText.at(i).setSmooth(true);
        sf::Sprite walk;
        walk.setOrigin(250,250);
        walk.setTexture(walkText.at(i));
        walk.setScale(0.4,0.4);
        walkSprite.push_back(walk);
    }

    // Textuurit hypylle
    path = "./sprites/BillyC/BC_Jump_";
    jumpFrames = 11;
    for (int i = 0; i < jumpFrames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture jump;
        if (!jump.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCJump" << i << ".png" << std::endl;
        }
        jumpText.push_back(jump);
    }
    for (unsigned int i = 0; i < jumpText.size(); ++i)
    {
        jumpText.at(i).setSmooth(true);
        sf::Sprite jump;
        jump.setOrigin(250,250);
        jump.setTexture(jumpText.at(i));
        jump.setScale(0.4,0.4);
        jumpSprite.push_back(jump);
    }

    path = "./sprites/BillyC/BC_Run_";
    runFrames = 6;
    setTextSprite(path, suffix, runFrames, RUN);

    std::cout << runSprite.size() << std::endl;

    // Asetetaan ly�nti 2
    path = "./sprites/BillyC/BC_Punch_2_";
    punch2Frames = 6;
    setTexturesSprites(path,suffix,punch2Frames,punch2Text,punch2Sprite,0.4,250,250);
}

BillyChing::BillyChing(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW) : Character(height, width, align, setX, setY, hp, screenH, screenW)
{
    /*
    sf::Texture standTex;
    if(!standTex.loadFromFile("./sprites/BillyC/BC_Stance_0.png"))
    {
        std::cout << "Ei loydy BC_Stance_0.png" << std::endl;
    }
    standingText.push_back(standTex);*/

//    standFrames = 17;
    standFrames = 8;

    /*
    for(unsigned int i = 0; i < 5; ++i)
    {
        sf::Texture test;
        if(!test.loadFromFile("./sprites/BillyC/BCStance0.png"))
        {
            std::cout << "Ttesti" << std::endl;
        }
        standingText.push_back(test);
    }*/




    // Asetetaan sprite tekstuurit
    std::string path = "./sprites/BillyC/BC_Stance_";
    std::string suffix = ".png";

    for(unsigned int a = 0; a < standFrames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture standTex;
        if(!standTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCStance0.png" << std::endl;
        }
        standingText.push_back(standTex);
    }


//    standFrames = standFrames + 5;


    // Asetetaan spritet
    for(unsigned int i = 0; i < standingText.size(); ++i)
    {
        standingText.at(i).setSmooth(true);

        sf::Sprite stand;
        stand.setOrigin(250,250);
        stand.setTexture(standingText.at(i));
        stand.setScale(0.4,0.4);
        standingSprite.push_back(stand);

    }

    // Asetetaan tekstuurit ly�nnille 1
    path = "./sprites/BillyC/BC_Punch";
    punch1Frames = 5;

    for (int a = 0; a < punch1Frames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture punchTex;
        if(!punchTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCPunch.png" << std::endl;
        }
//        punchTex.setSmooth(true);
        punch1Text.push_back(punchTex);

    }

    for (unsigned int i = 0; i < punch1Text.size(); ++i)
    {
        punch1Text.at(i).setSmooth(true);
        sf::Sprite punch;
        punch.setOrigin(250,250);
        punch.setTexture(punch1Text.at(i));
        punch.setScale(0.4,0.4);
        punch1Sprite.push_back(punch);
    }

    // Textuurit k�velylle
    path = "./sprites/BillyC/BC_Walk_";
    walkFrames = 8;
    for (int i = 0; i < walkFrames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture walk;
        if (!walk.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCWalk" << i << ".png" << std::endl;
        }
        walkText.push_back(walk);
    }
    for (unsigned int i = 0; i < walkText.size(); ++i)
    {
        walkText.at(i).setSmooth(true);
        sf::Sprite walk;
        walk.setOrigin(250,250);
        walk.setTexture(walkText.at(i));
        walk.setScale(0.4,0.4);
        walkSprite.push_back(walk);
    }

    // Textuurit hypylle
    path = "./sprites/BillyC/BC_Jump_";
    jumpFrames = 11;
    for (int i = 0; i < jumpFrames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture jump;
        if (!jump.loadFromFile(whole))
        {
            std::cout << "Ei loydy BCJump" << i << ".png" << std::endl;
        }
        jumpText.push_back(jump);
    }
    for (unsigned int i = 0; i < jumpText.size(); ++i)
    {
        jumpText.at(i).setSmooth(true);
        sf::Sprite jump;
        jump.setOrigin(250,250);
        jump.setTexture(jumpText.at(i));
        jump.setScale(0.4,0.4);
        jumpSprite.push_back(jump);
    }

    path = "./sprites/BillyC/BC_Run_";
    runFrames = 6;
    setTextSprite(path, suffix, runFrames, RUN);

    // Asetetaan ly�nti 2
    path = "./sprites/BillyC/BC_Punch_2_";
    punch2Frames = 6;
    setTexturesSprites(path,suffix,punch2Frames,punch2Text,punch2Sprite,0.4,250,250);
}

BillyChing::~BillyChing()
{
    //dtor
}


void BillyChing::drawCharacter(sf::RenderWindow* Window, sf::Time theClock)
{
    float timeIncrease = theClock.asSeconds();

    punchAika = punchAika + timeIncrease;
    standTime = standTime + timeIncrease;
    walkAika = walkAika + timeIncrease;
    runAika = runAika + timeIncrease;

    if (jumping == true)
    {
        standTime = 0;
        standPos = 0;
        walkAika = 0;
        walkPos = 0;
        punchAika = 0;
        punchPos = 0;
        runAika = 0;
        runPos = 0;

        float aikaPerFrame = startAirSpeed / 5;

        if (startAirSpeed - airSpeed >= aikaPerFrame * jumpPos)
        {
            jumpPos = jumpPos + 1;
        }

        if (jumpPos == jumpFrames)
        {
            jumpPos = jumpFrames - 1;
        }
        /*std::cout << aikaPerFrame << " : " << startAirSpeed << " : " << airSpeed << std::endl;

        if (airSpeed >= startAirSpeed - aikaPerFrame)
        {
            jumpPos = 0;
        }
        else if (airSpeed <= 25 and airSpeed >= -25)
        {
            std::cout << "hei" << std::endl;
            jumpPos = 5;
        }
        else
        {
            jumpPos = 6;
        }*/
        //std::cout << airSpeed << " : " << startAirSpeed << " - 5 = " << startAirSpeed - 5 << std::endl;
    }

    else if (attacking == true)
    {
        standTime = 0;
        standPos = 0;
        walkAika = 0;
        walkPos = 0;
        jumpPos = 0;
        runAika = 0;
        runPos = 0;

        if (firstAttack == false and (comboCounter <= 2))
        {
            if (punchAika > 0.05)
            {
                punchPos = punchPos + 1;
                punchAika = 0;

                if (punchPos >= punch1Frames)
                {
                    punchPos = 0;
                    punchAika = 0;
                }
            }
        }

        else if (comboCounter >= 3) {
            if (punchAika > 0.06)
            {
                punchPos = punchPos + 1;
                punchAika = 0;
                if (punchPos >= punch1Frames)
                {
                    punchPos = 0;
                    punchAika = 0;
                }
            }
        }

        if (punchPos == 0 and punchAika == 0)
        {
            attacking = false;
        }
    }

/*    std::cout << standPos << std::endl;
    std::cout << standTime << std::endl;*/

    else if (walking == true and running == false)
    {

        hitting = false;
        attacking = false;
        standTime = 0;
        standPos = 0;
        punchAika = 0;
        punchPos = 0;
        jumpPos = 0;
        runAika = 0;
        runPos = 0;

        if (walkAika > 0.1)
        {
            walkPos = walkPos + 1;
            walkAika = 0;
        }
        if (walkPos >= walkFrames)
        {
            walkPos = 0;
        }
    }

    else if (running == true)
    {
        standTime = 0;
        standPos = 0;
        punchAika = 0;
        punchPos = 0;
        jumpPos = 0;
        walkAika = 0;
        walkPos = 0;

        if (runAika > 0.1)
        {
            runPos = runPos + 1;
            runAika = 0;
        }
        if (runPos >= runFrames)
        {
            runPos = 0;
        }
    }

    else
    {
        hitting = false;
        attacking = false;
        punchAika = 0;
        punchPos = 0;
        walkAika = 0;
        walkPos = 0;
        jumpPos = 0;
        runAika = 0;
        runPos = 0;

        if (standTime > 0.1 and (standPos < 3 or standPos > 5))
        {
            standPos = standPos + 1;
            standTime = 0;
            if(standPos >= standFrames)
            {
                standPos = 0;
            }
        }
        else if(standTime > 0.15 and (standPos == 3 or standPos == 5))
        {
            standPos = standPos + 1;
            standTime = 0;
            if(standPos >= standFrames)
            {
                standPos = 0;
            }
        }
        else if(standTime > 0.2 and standPos == 4)
        {
            standPos = standPos + 1;
            standTime = 0;
            if(standPos >= standFrames)
            {
                standPos = 0;
            }
        }
    }


    // Piirrell��n spritet
    if (jumping == true)
    {
        if (lookingLeft == false)
        {
            jumpSprite.at(jumpPos).setPosition(posX + 60, posY - airPos + 70);
        }
        else
        {
            jumpSprite.at(jumpPos).setPosition(posX + 45, posY - airPos + 70);
        }
    }

    else if (attacking == true)
    {
        if (firstAttack == false and (comboCounter <= 2))
        {
            if (lookingLeft == false)
            {
                punch1Sprite.at(punchPos).setPosition(posX + 60, posY - airPos + 70);
            }
            else
            {
                punch1Sprite.at(punchPos).setPosition(posX + 45, posY - airPos + 70);
            }
        }
        else if (comboCounter >= 3)
        {
            if (lookingLeft == false)
            {
                punch2Sprite.at(punchPos).setPosition(posX + 60, posY - airPos + 70);
            }
            else
            {
                punch2Sprite.at(punchPos).setPosition(posX + 45, posY - airPos + 70);
            }
        }
    }

//    standingSprite.at(standPos).setPosition(posX - 40, posY - airPos - 30);

    else if (walking == true and running == false)
    {
        if (lookingLeft == false)
        {
            walkSprite.at(walkPos).setPosition(posX + 60, posY - airPos + 70);
        }
        else
        {
            walkSprite.at(walkPos).setPosition(posX + 45, posY - airPos + 70);
        }
    }
    else if (running == true)
    {
        if (lookingLeft == false)
        {
            runSprite.at(runPos).setPosition(posX + 60, posY - airPos + 70);
        }
        else
        {
            runSprite.at(runPos).setPosition(posX + 45, posY - airPos + 70);
        }
    }
    else
    {
        if (lookingLeft == false)
        {
            standingSprite.at(standPos).setPosition(posX + 60, posY - airPos + 70);
        }
        else
        {
            standingSprite.at(standPos).setPosition(posX + 45, posY - airPos + 70);
        }
    }

    if (jumping == true)
    {
        Window->draw(jumpSprite.at(jumpPos));
    }
    else if (attacking == true)
    {
        if (firstAttack == false and (comboCounter <= 2))
        {
            Window->draw(punch1Sprite.at(punchPos));
        }
        else if (comboCounter >= 3)
        {
            Window->draw(punch2Sprite.at(punchPos));
        }
    }
    else if (walking == true and running == false)
    {
        Window->draw(walkSprite.at(walkPos));
    }
    else if (running == true)
    {
        Window->draw(runSprite.at(runPos));
    }
    else
    {
        Window->draw(standingSprite.at(standPos));
    }

    if (attacking == true)
    {
        Window->draw(testPunch);
    }

    Character::drawCharacter(Window, theClock);

//    standingSprite.at(1).setPosition(posX + 300,posY - airPos);
  //  Window->draw(standingSprite.at(1));

  // Testing, some resetting
  running = false;
}

bool BillyChing::dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos)
{
    if (attacking == true)
    {
        if (comboCounter != 0 and comboTime.getElapsedTime().asSeconds() > 0.5)
        {
            comboCounter = 0;
        }

        if (firstAttack == false and comboCounter <= 2 and punchPos == 2 and hitting == false)
        {
            sf::RectangleShape tilapainen = sf::RectangleShape(sf::Vector2f(sizeW/5, sizeW/5));
            tilapainen.setFillColor(sf::Color(255,0,0,127));
            tilapainen.setOutlineColor(sf::Color::Black);
            tilapainen.setOutlineThickness(2);
            testPunch = tilapainen;

            if (lookingLeft == false)
            {
                if (comboCounter < 2) {
                    testPunch.setPosition(posX + sizeW, posY + sizeH/25);
                }
                else if(comboCounter == 2) {
                    testPunch.setPosition(posX + sizeW + sizeW/5, posY + sizeH/6.5);
                }
            }
            else
            {
                if (comboCounter < 2) {
                    testPunch.setPosition(posX - sizeW/5, posY + sizeH/25);
                }
                else if (comboCounter == 2) {
                    testPunch.setPosition(posX - sizeW/5 - sizeW/7, posY + sizeH/6.5);
                }
            }

            danger = testPunch;
            if (comboCounter < 2) damage = 80;
            else if (comboCounter == 2) damage = 110;
            playerPosY = posY + sizeH;

            hitting = true;

            healthPos = maxHealth.getPosition().x;

            comboCounter = comboCounter + 1;
            if (comboCounter > 2) {
                punchPos = 0;
            }
            comboTime.restart();

            return true;
        }
    }
    return false;
}

void BillyChing::attack()
{
    if (grappling == true)
    {
        throwTarget();
    }

    else if (attacking == false)
    {
        swing.play();
        attacking = true;
    }
}

void BillyChing::throwTarget()
{
    if (throwDir == YLOS)
    {
        std::cout << "tomoe " << grabTarget->getHeight() << std::endl;
    }
    else if (throwDir == ALAS)
    {
        std::cout << "tenshinage" << std::endl;
    }
    else if ((lookingLeft == true and throwDir == VASEN) or (lookingLeft == false and throwDir == OIKEA))
    {
        SUUNTA flyDir = EIOLE;

        if (lookingLeft == true)
        {
            flyDir = VASEN;
        }
        else
        {
            flyDir = OIKEA;
        }
        // Direction, power, airpower
        grabTarget->startFlying(flyDir,1000,500);

        grappling = false;
        grabTarget->setGrabbled(false);
    }
    else if ((lookingLeft == true and throwDir == OIKEA) or (lookingLeft == false and throwDir == VASEN))
    {
        SUUNTA flyDir = EIOLE;
        if (lookingLeft == true)
        {
            flyDir = OIKEA;
        }
        else
        {
            flyDir = VASEN;
        }

        grabTarget->startFlying(flyDir,500,250);

        grappling = false;
        grabTarget->setGrabbled(false);

        //std::cout << "kotegaeshi" << std::endl;
    }
    else
    {
        std::cout << "atemi" << std::endl;
    }
}

void BillyChing::updatePunching(float graviy, float timeStuff)
{

}
