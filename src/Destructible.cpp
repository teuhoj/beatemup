#include "Destructible.h"

Destructible::Destructible(float xKoord, float yKoord, int jarj, LOOKS outside, DESTSPAWN sisalto) : Object(xKoord, yKoord, jarj)
{
    shell = outside;

    if (shell == TRASHCAN)
    {
        width = 75;
        height = 120;

        if (!tekstuuri.loadFromFile("./sprites/Items/Trash_can.png"))
        {
            std::cout << "Ei loydy Trash_can.png" << std::endl;
        }
        tekstuuri.setSmooth(true);
        kuva.setTexture(tekstuuri);
        kuva.setOrigin(62,100);
        kuva.setScale(0.5,0.5);
        kuva.setPosition(posX, posY);
    }


    hitbox = sf::RectangleShape(sf::Vector2f(width, height));
    hitbox.setFillColor(sf::Color(96,96,96,107));
    hitbox.setPosition(posX, posY);

    shadow.setPosition(posX - width/4,posY + height - width/4);
    shadow.setFillColor(sf::Color(10,10,10,100));
    shadow.setRadius(width/2);
    shadow.scale(1.5f,0.5f);


    //ctor
    health = 50;

    type = BREAKABLE;
    amountHealed = -1;

    contains = sisalto;


}

Destructible::~Destructible()
{
    //dtor
}

void Destructible::takeDamage(sf::RectangleShape danger, float damage, float hitOriginY)
{
    if((posX < danger.getPosition().x and posX + width > danger.getPosition().x) or
       (posX < danger.getPosition().x + danger.getLocalBounds().width and posX + width > danger.getPosition().x + danger.getLocalBounds().width))
    {

        if (hitOriginY > posY + height - 20 and hitOriginY < posY + height + 20)
        {
//            lightHit.play();

//            showHealthPos = healthPos;

            health = health - damage;
            if (health > 0)
            {
                std::cout << health << std::endl;
            }
            else
            {
                std::cout << type << " kuoli, " << health << std::endl;
            }
        }
    }
}

DESTSPAWN Destructible::getContents()
{
    return contains;
}

float Destructible::getHealth()
{
    return health;
}

void Destructible::drawObject(sf::RenderWindow* Window)
{
    kuva.setPosition(posX + width/2, posY + height/2 + 13);
    Window->draw(kuva);
    Object::drawObject(Window);
}
