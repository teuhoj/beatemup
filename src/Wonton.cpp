#include "Wonton.h"

Wonton::Wonton(float xKoord, float yKoord, int jarj) : Object(xKoord, yKoord, jarj)
{
    //ctor
    type = HEALING;

    width = 25;
    height = 25;

    hitbox = sf::RectangleShape(sf::Vector2f(25,25));
    hitbox.setFillColor(sf::Color(255,178,102,107));

    hitbox.setPosition(posX, posY);

    amountHealed = 200;

    shadow.setPosition(posX - width/4,posY + height - width/4);
    shadow.setFillColor(sf::Color(10,10,10,100));
    shadow.setRadius(width/2);
    shadow.scale(1.5f,0.5f);

    if (!tekstuuri.loadFromFile("./sprites/Items/Wonton.png"))
    {
        std::cout << "Ei loydy Wonton.png" << std::endl;
    }
    tekstuuri.setSmooth(true);
    kuva.setTexture(tekstuuri);
    kuva.setOrigin(50,29);
    kuva.setScale(0.5,0.5);
    kuva.setPosition(posX + 18, posY + 12);
}

Wonton::~Wonton()
{
    //dtor
}

void Wonton::drawObject(sf::RenderWindow* Window)
{
    kuva.setPosition(posX + 18, posY + 12 - airPos);
    Window->draw(kuva);
    Object::drawObject(Window);
}
