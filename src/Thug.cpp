#include "Thug.h"

Thug::Thug(int align, float setX, float setY, float hp, float screenH, float screenW) : Character(align, setX, setY, hp, screenH, screenW)
{
    //ctor

    // Polut
    std::string path = "./sprites/Thug/Idle_";
    std::string suffix = ".png";
    standFrames = 1;

    // Tekstuurit
    for (unsigned int i = 0; i < standFrames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture standTex;
        if(!standTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy ThugIdle.png" << std::endl;
        }
        standingText.push_back(standTex);
    }
    // Spritet
    for (unsigned int i = 0; i < standingText.size(); ++i)
    {
        standingText.at(i).setSmooth(true);

        sf::Sprite stand;
        stand.setOrigin(250,250);
        stand.setTexture(standingText.at(i));
        stand.setScale(0.4,0.4);
        standingSprite.push_back(stand);
    }

    sinceLastBehaviour = 0;

    behaviourSelect.restart();
    currentBehaviour = ODOTA;
    lookingLeft = true;
}

Thug::Thug(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW) : Character(height, width, align, setX, setY, hp, screenH, screenW)
{
    // Polut
    std::string path = "./sprites/Thug/Idle_";
    std::string suffix = ".png";
    standFrames = 4;

    // Tekstuurit
    for (unsigned int i = 0; i < standFrames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture standTex;
        if(!standTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy ThugIdle.png" << std::endl;
        }
        standingText.push_back(standTex);
    }
    // Spritet
    for (unsigned int i = 0; i < standingText.size(); ++i)
    {
        standingText.at(i).setSmooth(true);

        sf::Sprite stand;
        stand.setOrigin(250,250);
        stand.setTexture(standingText.at(i));
        stand.setScale(0.4,0.4);
        standingSprite.push_back(stand);
    }


    sinceLastBehaviour = 0;
    behaviourSelect.restart();
    currentBehaviour = ODOTA;

    lookingLeft = true;
}

Thug::~Thug()
{
    //dtor
}

void Thug::act(float limitPosY, float limitMaxY, float limitPosX, float limitMaxX, float timeMod, float player1posY, float player1posX)
{
    if (active == true)
    {
        inCurrentBehaviour = behaviourSelect.restart();
        sinceLastBehaviour = sinceLastBehaviour + inCurrentBehaviour.asSeconds();

        if (sinceLastBehaviour >= 2)
        {
            currentBehaviour = static_cast<BEHAVIOUR>(rand() % last);

        /*
        if (currentBehaviour == ODOTA)
        {
            std::cout << "Odotetaan..." << std::endl;
        }
        else if (currentBehaviour == KIERRA)
        {
            std::cout << "Flanking force on the move..." << std::endl;
        }
        else if (currentBehaviour == LAHESTY)
        {
            std::cout << "CHAARGE" << std::endl;
        }*/


            sinceLastBehaviour = 0;
        }

        if (currentBehaviour == KIERRA)
        {
            if (posY + sizeH > player1posY + 5)
            {
//          std::cout << "Ylos" << std::endl;
                moveCharacter(YLOS, 150 * timeMod, limitPosY, limitMaxY, limitPosX, limitMaxX, false);
            }
            else if (posY + sizeH < player1posY - 5)
            {
//          std::cout << "Alas" << std::endl;
                moveCharacter(ALAS, 150 * timeMod, limitPosY, limitMaxY, limitPosX, limitMaxX, false);
            }
        }
        else if(currentBehaviour == LAHESTY)
        {
            if (posX > player1posX + 5)
            {
                moveCharacter(VASEN, 150 * timeMod, limitPosY, limitMaxY, limitPosX, limitMaxX, false);
            }
            else if (posX < player1posX -5)
            {
                moveCharacter(OIKEA, 150 * timeMod, limitPosY, limitMaxY, limitPosX, limitMaxX, false);
            }
        }
    }
}

void Thug::drawCharacter(sf::RenderWindow* Window, sf::Time theClock)
{
    if (active == true)
    {
        standTime = standTime + theClock.asSeconds();

        if (standTime > 0.4 and standPos == 0)
        {
            standPos = 1;
            standTime = 0;
        }
        else if (standTime > 0.13 and standPos != 0)
        {
            standPos = standPos + 1;
            standTime = 0;
        }

        if (standPos == standingSprite.size())
        {
            standPos = 0;
        }

        standingSprite.at(standPos).setPosition(posX + 30, posY - airPos + 70);

//        standingSprite.at(standPos).setColor(sf::Color(255,200,200));

        Window->draw(standingSprite.at(standPos));

        Character::drawCharacter(Window, theClock);
    }
}
