#include "Player.h"

Player::Player()
{
    //ctor
    Billy = 0;
    Kaiser = 0;
    Chara = 0;
    myChoice = NOTSELECTED;
}

Player::~Player()
{
    //dtor
    delete Billy;
    Billy = 0;
    delete Kaiser;
    Kaiser = 0;
    delete Chara;
    Chara = 0;
}

void Player::selectBilly(float screenH, float screenW, int type)
{
    Billy = new BillyChing(160,100,type,5,(screenH/3)*2, 1000, screenH, screenW);
    myChoice = BILLY;
}

void Player::selectKaiser(float screenH, float screenW, int type)
{
    Kaiser = new KaiserChef(160,100,type,5,(screenH/3)*2, 1000, screenH, screenW);
    myChoice = KAISER;
}

void Player::setCharacterPosition(float xKoord, float yKoord)
{
    if (myChoice == BILLY)
    {
        Billy->setPosition(xKoord, yKoord);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->setPosition(xKoord, yKoord);
    }
}

void Player::resetLifePosition(float screenH, float screenW)
{
    if (myChoice == BILLY)
    {
        Billy->resetLifePos(screenW, screenH);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->resetLifePos(screenW, screenH);
    }
}

void Player::resetCharacter()
{
    if (myChoice == BILLY)
    {
        Billy->resetCharacter();
    }
    else if (myChoice == KAISER)
    {
        Kaiser->resetCharacter();
    }
}

void Player::moveCharacter(SUUNTA direc, float amount, float limitPosY, float limitMaxY, float limitPosX, float limitMaxX, bool keepRunning)
{
    if (myChoice == BILLY)
    {
        Billy->moveCharacter(direc, amount, limitPosY, limitMaxY, limitPosX, limitMaxX, keepRunning);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->moveCharacter(direc, amount, limitPosY, limitMaxY, limitPosX, limitMaxX, keepRunning);
    }
}

void Player::jumpCharacter(float liftoff)
{
    if (myChoice == BILLY)
    {
        Billy->jumpCharacter(liftoff);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->jumpCharacter(liftoff);
    }
}

void Player::attackCharacter()
{
    if (myChoice == BILLY)
    {
        Billy->attack();
    }
    else if (myChoice == KAISER)
    {
        Kaiser->attack();
    }
}

void Player::checkEnemyProximity(Character* enemy)
{
    if (myChoice == BILLY)
    {
        Billy->checkEnemyProximity(enemy);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->checkEnemyProximity(enemy);
    }
}

bool Player::checkItemProximity(Object* item)
{
    if(myChoice == BILLY)
    {
        return Billy->checkItemProximity(item);
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->checkItemProximity(item);
    }
    else {
        return false;
    }
}
bool Player::isCharacterAttacking()
{
    if (myChoice == BILLY)
    {
        return Billy->isAttacking();
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->isAttacking();
    }
}

bool Player::isCharacterJumping()
{
    if (myChoice == BILLY)
    {
        return Billy->isJumping();
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->isJumping();
    }
}

bool Player::dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos)
{
    if (myChoice == BILLY)
    {
        return Billy->dangerZone(danger, damage, playerPosY, healthPos);
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->dangerZone(danger, damage, playerPosY, healthPos);
    }
}

void Player::updateCharacter(float gravity, float timeStuff, float airFriction)
{
    if (myChoice == BILLY)
    {
        Billy->updateCharacter(gravity, timeStuff, airFriction);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->updateCharacter(gravity, timeStuff, airFriction);
    }
}

void Player::updatePunching(float gravity, float timeStuff)
{
    if (myChoice == BILLY)
    {
        Billy->updatePunching(gravity, timeStuff);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->updatePunching(gravity, timeStuff);
    }
}

void Player::drawCurrentCharacter(sf::RenderWindow* Window, sf::Time theClock)
{
    if (myChoice == BILLY)
    {
        Billy->drawCharacter(Window, theClock);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->drawCharacter(Window, theClock);
    }
}

void Player::drawLife(sf::RenderWindow* Window, float movex, float screenH)
{
    if (myChoice == BILLY)
    {
        Billy->drawLife(Window, movex, screenH);
    }
    else if (myChoice == KAISER)
    {
        Kaiser->drawLife(Window, movex, screenH);
    }
}

Character* Player::returnCurrentCharacter()
{
    if (myChoice == BILLY)
    {
        return Billy;
    }
    else if (myChoice == KAISER)
    {
        return Kaiser;
    }
    else
    {
        std::cout << "something has gone wrong" << std::endl;
    }
}

float Player::getPosY()
{
    if (myChoice == BILLY)
    {
        return Billy->getPosY();
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->getPosY();
    }
}
float Player::getPosX()
{
    if (myChoice == BILLY)
    {
        return Billy->getPosX();
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->getPosX();
    }
}

float Player::getHeight()
{
    if (myChoice == BILLY)
    {
        return Billy->getHeight();
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->getHeight();
    }
}

float Player::getWidth()
{
    if (myChoice == BILLY)
    {
        return Billy->getWidth();
    }
    else if (myChoice == KAISER)
    {
        return Kaiser->getWidth();
    }
}
