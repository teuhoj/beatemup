#include "KaiserChef.h"

KaiserChef::KaiserChef(int align, float setX, float setY, float hp, float screenH, float screenW) : Character(align, setX, setY, hp, screenH, screenW)
{
    //ctor
    std::string path = "./sprites/KaiserC/Stand_";
    std::string suffix = ".png";

    standFrames = 10;

    for(unsigned int a = 0; a < standFrames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture standTex;
        if(!standTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy KCStance.png" << std::endl;
        }
        standingText.push_back(standTex);
    }

    // Asetetaan spritet
    for(unsigned int i = 0; i < standingText.size(); ++i)
    {
        standingText.at(i).setSmooth(true);

        sf::Sprite stand;
        stand.setOrigin(250,250);
        stand.setTexture(standingText.at(i));
        stand.setScale(0.4,0.4);
        standingSprite.push_back(stand);
    }

    // Steam spritet
    drawSteam = false;
    steamTime = 0;
    steamPos = 0;
    path = "./sprites/KaiserC/Steam_";
    suffix = ".png";
    steamFrames = 10;
    for (unsigned int a = 1; a <= steamFrames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture vaporTex;
        if (!vaporTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy steam.png" << std::endl;
        }
        steamText.push_back(vaporTex);
    }
    for(unsigned int i = 0; i < steamText.size(); ++i)
    {
        steamText.at(i).setSmooth(true);
        sf::Sprite steam;
        steam.setOrigin(250,250);
        steam.setTexture(steamText.at(i));
        steam.setScale(0.4,0.4);
        steamSprite.push_back(steam);
    }
}

KaiserChef::KaiserChef(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW) : Character(height, width, align, setX, setY, hp, screenH, screenW)
{
    //ctor
    std::string path = "./sprites/KaiserC/Stand_";
    std::string suffix = ".png";

    standFrames = 10;

    for(unsigned int a = 0; a < standFrames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture standTex;
        if(!standTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy KCStance.png" << std::endl;
        }
        standingText.push_back(standTex);
    }

    // Asetetaan spritet
    for(unsigned int i = 0; i < standingText.size(); ++i)
    {
        standingText.at(i).setSmooth(true);

        sf::Sprite stand;
        stand.setOrigin(250,250);
        stand.setTexture(standingText.at(i));
        stand.setScale(0.4,0.4);
        standingSprite.push_back(stand);
    }

    // Steam spritet
    drawSteam = false;
    steamTime = 0;
    steamPos = 0;
    path = "./sprites/KaiserC/Steam_";
    suffix = ".png";
    steamFrames = 10;
    for (unsigned int a = 1; a <= steamFrames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture vaporTex;
        if (!vaporTex.loadFromFile(whole))
        {
            std::cout << "Ei loydy steam.png" << std::endl;
        }
        steamText.push_back(vaporTex);
    }
    for(unsigned int i = 0; i < steamText.size(); ++i)
    {
        steamText.at(i).setSmooth(true);
        sf::Sprite steam;
        steam.setOrigin(250,250);
        steam.setTexture(steamText.at(i));
        steam.setScale(0.4,0.4);
        steamSprite.push_back(steam);
    }
}

KaiserChef::~KaiserChef()
{
    //dtor
}

void KaiserChef::drawCharacter(sf::RenderWindow* Window, sf::Time theClock)
{
    sf::Sprite toDraw = standingSprite.at(0);
    sf::Sprite steamToDraw = steamSprite.at(0);

    float timeincrease = theClock.asSeconds();
    if (false)
    {

    }
    else
    {
        standTime = standTime + timeincrease;

        if (standTime >= 0.15)
        {
            standPos = standPos + 1;
            standTime = 0;
        }

        if (standPos >= standFrames)
        {
            standPos = 0;
        }
        toDraw = standingSprite.at(standPos);

        if (standPos >= 6)
        {
            drawSteam = true;
        }
    }

    // Lasketaan oikea h�yry piirrett�v�ksi
    if (drawSteam == true)
    {
        drawSteam = true;
        steamTime = steamTime + timeincrease;
        float targetTime = (0.15 * 4) / steamFrames;
        if (steamTime >= targetTime) {
            steamPos = steamPos + 1;
            steamTime = 0;
        }
        if (steamPos >= steamFrames)
        {
            steamPos = 0;
            steamTime = 0;
            drawSteam = false;
        }
        else {
            steamToDraw = steamSprite.at(steamPos);
        }
    }

    toDraw.setPosition(posX + 60, posY - airPos + 70);
    Window->draw(toDraw);
    if (drawSteam == true)
    {
        steamToDraw.setPosition(posX + 162, posY - airPos + 85);
        Window->draw(steamToDraw);
    }

//    Character::drawCharacter(Window, theClock);
}

bool KaiserChef::dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos)
{

}

void KaiserChef::attack()
{

}

void KaiserChef::throwTarget()
{

}

void KaiserChef::updatePunching(float graviy, float timeStuff)
{

}
