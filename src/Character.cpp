#include "Character.h"
/*Character::Character()
{
    std::cout << "Heip" << std::endl;
}*/

// Muista siirt�� j�lkeenp�in kaikki alustukset yksinkertaiseen rakentajaan (ylh��ll�)
Character::Character(int align, float setX, float setY, float hp, float screenH, float screenW)
{
    //ctor
    sizeW = 20.0;
    sizeH = 80.0;

    posX = setX;
    posY = setY;

    airPos = 0;
    airSpeed = 0;

    hitBox = sf::RectangleShape(sf::Vector2f(sizeW, sizeH));
    punching = sf::RectangleShape(sf::Vector2f(0, sizeW/5));
    maxPunchLength = sizeH/3;
    punchLength = 0;
    pullPunch = false;

//    punching = sf::RectangleShape(sf::Vector2f(300,500));

    type = align;


    health = hp;
    stamina = hp;
    barSize = screenW/2 - 40;

    currentHealth = sf::RectangleShape(sf::Vector2f(barSize ,screenH/30));
    currentHealth.setFillColor(sf::Color::Yellow);
    maxHealth = sf::RectangleShape(sf::Vector2f(barSize, screenH/30));
    maxHealth.setOutlineThickness(5);
    maxHealth.setFillColor(sf::Color::Red);
    maxHealth.setOutlineColor(sf::Color::Black);

    setHitColor(screenW, screenH);

    shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
    shadow.setRadius(sizeW/2);
    shadow.scale(1.5f,0.5f);

    jumping = false;

    if(!swingBuffer.loadFromFile("./audio/whoosh.flac"))
    {
        std::cout << "Ei l�ydy whoosh" << std::endl;
    }
    else
    {
        swing.setBuffer(swingBuffer);
    }
    if(!lightHitBuffer.loadFromFile("./audio/light_hit.wav"))
    {
        std::cout << "Ei l�ydy light_hit" << std::endl;
    }
    else
    {
        lightHit.setBuffer(lightHitBuffer);
    }

    attacking = false;
//    flip = 0;
    standFrames = 0;
    standPos = 0;
    standTime = 0;

    punchPos = 0;
    punchAika = 0;

    firstAttack = false;

    if (type <= 1)
    {
        lookingLeft = false;
        active = true;
    }
    else
    {
        lookingLeft = true;
        active = false;
    }

    hitting = false;
    walking = false;
    running = false;

    showHealthPos = -1;

    grappling = false;
    grappled = false;
    totalGrappleTime = 0;

    throwDir = EIOLE;
    grabTarget = 0;
    flying = false;
    flyingPower = 0;
    flyingSpeed = 0;
    flyDir = EIOLE;
    flyingPos = 0;

    jumpPos = 0;

    runPos = 0;
    runAika = 0;

    comboCounter = 0;
    comboTimeSeconds = 0;
    comboTime.restart();
}

Character::Character(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW)
{
    sizeW = width;
    sizeH = height;

    posX = setX;
    posY = setY;

    airPos = 0;
    airSpeed = 0;

    hitBox = sf::RectangleShape(sf::Vector2f(sizeW, sizeH));
    // Testaillaan ly�ntej�
    punching = sf::RectangleShape(sf::Vector2f(0, sizeW/5));
    maxPunchLength = sizeH/3;
    punchLength = 0;
    pullPunch = false;

//    punching = sf::RectangleShape(sf::Vector2f(300,500));

    type = align;

    health = hp;
    stamina = hp;
    barSize = screenW/2 - 40;

    currentHealth = sf::RectangleShape(sf::Vector2f(barSize ,screenH/30));
    currentHealth.setFillColor(sf::Color::Yellow);
    maxHealth = sf::RectangleShape(sf::Vector2f(barSize, screenH/30));
    maxHealth.setOutlineThickness(5);
    maxHealth.setFillColor(sf::Color::Red);
    maxHealth.setOutlineColor(sf::Color::Black);

    setHitColor(screenW, screenH);

    shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
    shadow.setRadius(sizeW/2);
    shadow.scale(1.5f,0.5f);

    //health = hp;

    jumping = false;

    if(!swingBuffer.loadFromFile("./audio/whoosh.flac"))
    {
        std::cout << "Ei l�ydy whoosh" << std::endl;
    }
    else
    {
        swing.setBuffer(swingBuffer);
    }
    if(!lightHitBuffer.loadFromFile("./audio/light_hit.wav"))
    {
        std::cout << "Ei l�ydy light_hit" << std::endl;
    }
    else
    {
        lightHit.setBuffer(lightHitBuffer);
    }

    attacking = false;
    //flip = 0;
    standFrames = 0;
    standPos = 0;
    standTime = 0;

    walkPos = 0;
    walkAika = 0;

    punchPos = 0;
    punchAika = 0;

    firstAttack = false;

    if (type <= 1)
    {
        lookingLeft = false;
        active = true;
    }
    else
    {
        lookingLeft = true;
        active = false;
    }

    hitting = false;
    walking = false;
    running = false;

    showHealthPos = -1;

    grappling = false;
    grappled = false;
    totalGrappleTime = 0;

    throwDir = EIOLE;
    grabTarget = 0;
    flying = false;
    flyingPower = 0;
    flyingSpeed = 0;
    flyDir = EIOLE;
    flyingPos = 0;

    jumpPos = 0;

    runPos = 0;
    runAika = 0;

    comboCounter = 0;
    comboTimeSeconds = 0;
    comboTime.restart();
}

Character::~Character()
{
    //dtor
}

void Character::resetCharacter()
{
    walking = false;
}

// Hypyss� hahmolle asetetaan l�ht�nopeus sek� jumping == true
void Character::jumpCharacter(float liftOff)
{
    if (grappled == true or grappling == true)
    {
        return;
    }

    if (jumping == false)
    {
        airSpeed = liftOff;
        startAirSpeed = liftOff;
        jumping = true;
    }
//    std::cout << airSpeed << std::endl;
}

void Character::moveCharacter(SUUNTA direc, float amount, float limitPosY, float limitMaxY, float limitPosX, float limitMaxX, bool keepRunning)
{
    if (keepRunning == true)
    {
        running = true;
    }
    else
    {
        running = false;
    }

    if (grappled == true or grappling == true)
    {
        throwDir = direc;
        return;
    }

    if (flying == true)
    {
        return;
    }

    if (attacking == false or airPos >= 0)
    {
        walking = true;

        // Liikkeet, yl�s...
        if(direc == YLOS)
        {
            hitBox.move(0,- amount);
            shadow.move(0,- amount);
            posY = posY - amount;

            // Rajataan yl�sp�in suuntautuva liike kent�n alueeseen
            if (posY + sizeH < limitPosY)
            {
                posY = limitPosY - sizeH;
                hitBox.setPosition(posX, posY);
                shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
            }
        }
        // ... alas ...
        else if(direc == ALAS)
        {
            hitBox.move(0, amount);
            shadow.move(0, amount);
            posY = posY + amount;

            if (posY + sizeH > limitMaxY)
            {
                posY = limitMaxY - sizeH;
                hitBox.setPosition(posX, posY);
                shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
            }
            // Liike tulisi rajata alareunaan
        }
        // ... oikealle ...
        else if(direc == OIKEA)
        {
            if (lookingLeft == true)
            {
                lookingLeft = false;
                for (unsigned int i = 0; i < standingSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        standingSprite.at(i).setScale(0.4,0.4);
                    }
                    else
                    {
                        standingSprite.at(i).setScale(-0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < punch1Sprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        punch1Sprite.at(i).setScale(0.4,0.4);
                    }
                    else
                    {
                        punch1Sprite.at(i).setScale(-0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < walkSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        walkSprite.at(i).setScale(0.4,0.4);
                    }
                    else
                    {
                        walkSprite.at(i).setScale(-0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < jumpSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        jumpSprite.at(i).setScale(0.4,0.4);
                    }
                    else
                    {
                        jumpSprite.at(i).setScale(-0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < runSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        runSprite.at(i).setScale(0.4,0.4);
                    }
                    else
                    {
                        runSprite.at(i).setScale(-0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < punch2Sprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        punch2Sprite.at(i).setScale(0.4,0.4);
                    }
                    else
                    {
                        punch2Sprite.at(i).setScale(-0.4,0.4);
                    }
                }
            }

            hitBox.move(amount, 0);
            shadow.move(amount, 0);
            posX = posX + amount;

            if (posX + sizeW > limitMaxX)
            {
                posX = limitMaxX - sizeW;
                hitBox.setPosition(posX, posY);
                shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
            }
            // Liike tulisi rajata oikeaan reunaan
        }
        // ... ja vasemmalle
        else if(direc == VASEN)
        {
            if (lookingLeft == false)
            {
                lookingLeft = true;
                for (unsigned int i = 0; i < standingSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        standingSprite.at(i).setScale(-0.4,0.4);
                    }
                    else
                    {
                        standingSprite.at(i).setScale(0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < punch1Sprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        punch1Sprite.at(i).setScale(-0.4,0.4);
                    }
                    else
                    {
                        punch1Sprite.at(i).setScale(0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < walkSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        walkSprite.at(i).setScale(-0.4,0.4);
                    }
                    else
                    {
                        walkSprite.at(i).setScale(0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < jumpSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        jumpSprite.at(i).setScale(-0.4,0.4);
                    }
                    else
                    {
                        jumpSprite.at(i).setScale(0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < runSprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        runSprite.at(i).setScale(-0.4,0.4);
                    }
                    else
                    {
                        runSprite.at(i).setScale(0.4,0.4);
                    }
                }
                for (unsigned int i = 0; i < punch2Sprite.size(); ++i)
                {
                    if (type <= 1)
                    {
                        punch2Sprite.at(i).setScale(-0.4,0.4);
                    }
                    else
                    {
                        punch2Sprite.at(i).setScale(0.4,0.4);
                    }
                }
            }

            hitBox.move(- amount, 0);
            shadow.move(- amount, 0);
            posX = posX - amount;

            if (posX < limitPosX)
            {
                posX = limitPosX;
                hitBox.setPosition(posX, posY);
                shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
            }

            // Liike tulisi rajata vasempaan reunaan
        }
    }
}

// Hy�kk�ys
void Character::attack()
{
    if(attacking == false and firstAttack == false)
    {
//        std::cout << "Attack!" << std::endl;
        attacking = true;
        swing.play();
    }

    if (firstAttack == true and attacking == false and punchTime.getElapsedTime().asSeconds() < 0.5)
    {
        attacking = true;
        swing.play();
    }
}

// Kiinnipidosta heitto
void Character::throwTarget()
{

}

// Funktio vaaran asettamiselle
bool Character::dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos)
{
    if(attacking == true)
    {
//        if(punchLength <= maxPunchLength + 1 and punchLength > maxPunchLength - 5)
        if (punchLength >= maxPunchLength or punchLength <= - maxPunchLength)
        {
            if (lookingLeft == false)
            {
                punchLength = maxPunchLength;
            }
            else
            {
                punchLength = - maxPunchLength;
            }

            sf::RectangleShape damageArea = sf::RectangleShape(sf::Vector2f(5, sizeW/5 + 3));

//            sf::RectangleShape damageArea = sf::RectangleShape(sf::Vector2f(300, 400));

            if (lookingLeft == false)
            {
                damageArea.setPosition(posX + sizeW + punchLength, posY + sizeH/7 - 1.5);
            }
            else
            {
                damageArea.setPosition(posX + punchLength - 5, posY + sizeH/7 - 1.5);
            }

//            damageArea.setPosition(100, 200);

            damageArea.setFillColor(sf::Color::Red);
            danger = damageArea;
            damage = 100;

//            std::cout << "hello" << std::endl;
            playerPosY = posY + sizeH;
            healthPos = maxHealth.getPosition().x;

            return true;
        }
    }
    return false;
}

void Character::takeHit(sf::RectangleShape danger, float damage, float hitOriginY, float healthPos)
{
    if((posX < danger.getPosition().x and posX + sizeW > danger.getPosition().x) or
       (posX < danger.getPosition().x + danger.getLocalBounds().width and posX + sizeW > danger.getPosition().x + danger.getLocalBounds().width))
    {

        if (hitOriginY > posY + sizeH - 20 and hitOriginY < posY + sizeH + 20)
        {
            lightHit.play();

            showHealthPos = healthPos;

            health = health - damage;
            if (health > 0)
            {
                std::cout << health << std::endl;
            }
            else
            {
                std::cout << type << " kuoli, " << health << std::endl;
            }
        }
    }
}

// P�ivitt�� hyppytilanteen
void Character::updateCharacter(float gravity, float timeStuff, float airFriction)
{
    throwDir = EIOLE;

    if ((grappled == true or grappling == true) and totalGrappleTime <= 5)
    {
        sf::Time grabTime = grappleClock.restart();
        totalGrappleTime = totalGrappleTime + grabTime.asSeconds();
    }
    if ((grappled == true or grappling == true) and totalGrappleTime > 5)
    {
        totalGrappleTime = 0;
        grappled = false;
        grappling = false;
        grabTarget = 0;
    }
    /*if (type == 0)
    {
        std::cout << totalGrappleTime << std::endl;
    }*/

    // Jos ollaan hypyss�
    if (jumping == true)
    {
        // Lasketaan liikenopeutta
        airSpeed = airSpeed - gravity;
        // Muokataan ilmasijaintia nopeuden mukaisesti
        airPos = airPos + airSpeed * timeStuff;
        // Pienennet��n varjoa hypyn korkeuteen n�hden
        // Jos ollaan yli maksimaalisen rajan, ei pienennet� varjoa enemp��
        float maksimaalista = airPos/20;
        if(sizeW/2 - maksimaalista >= 0)
        {
            shadow.setRadius(sizeW/2 - maksimaalista);
        }

        shadow.setPosition(posX - sizeW/4 + airPos/15,posY + sizeH - sizeW/4);

//        flip = flip + 410 * timeStuff;
//        std::cout << punch.getPosition().x << " " << punch.getPosition().y << std::endl;
    }

    if (posY - airPos > posY)
    {
        //flip = 0;

        jumping = false;
        airPos = 0;
        airSpeed = 0;
        shadow.setRadius(sizeW/2);
        shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
//        shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
    }

    if (flying == true)
    {
        flyingSpeed = flyingSpeed - airFriction;
//        airSpeed = airSpeed - airFriction;
        flyingPos = flyingPos +  flyingSpeed * timeStuff;

        //std::cout << posX << std::endl;

        if (flyingSpeed <= 0)
        {
//            flying = false;
            flyingSpeed = 0;
            flyingPos = 0;
        }

//        float flyingVara = flyingSpeed;
        float flyingVara = flyingPos;
        //std::cout << flyDir << std::endl;
        if (flyDir == VASEN)
        {
            flyingVara = flyingVara * -1;
        }
        //std::cout << flyingVara << std::endl;
        posX = posX + flyingVara;
        flyingPos = 0;
    }

    // Otetaan liikkumattomuus pois, T�LL� HETKELL� KATSOTAAN VAIN ETT� EI OLLA ILMASSA EIK� LENT�M�SS� HEITOSTA
    if (flying == true and flyingSpeed <= 0 and airSpeed == 0)
    {
        flying = false;
        /*float varaH = sizeH;
        sizeH = sizeW;
        sizeW = varaH;
        hitBox = sf::RectangleShape(sf::Vector2f(sizeW, sizeH));*/
    }


    /*
    if(type == 0)
    {
        hitBox.setRotation(flip);
    }
    else
    {
        hitBox.setRotation(-flip);
    }*/

//    updatePunching(gravity, timeStuff);
}

void Character::updatePunching(float gravity, float timeStuff)
{
    if (attacking == true)
    {
//        std::cout << "hello" << std::endl;
        if (lookingLeft == false)
        {
            if (pullPunch == false)
            {
                punchLength = punchLength + timeStuff * 300;
    //            std::cout << "pullpunch false " << punchLength << std::endl;
            }
            else if(pullPunch == true)
            {
                punchLength = punchLength - timeStuff * 300;
    //            std::cout << "pullpunch true " << punchLength << std::endl;
            }
        }
        else
        {
            if (pullPunch == false)
            {
                punchLength = punchLength - timeStuff * 300;
            }
            else
            {
                punchLength = punchLength + timeStuff * 300;
            }
        }

        if (pullPunch == false and (punchLength > maxPunchLength or punchLength < -maxPunchLength))
        {
//            std::cout << "pull the punch" << std::endl;
            pullPunch = true;
        }
        else if((lookingLeft == false and pullPunch == true and punchLength < 0) or (lookingLeft == true and pullPunch == true and punchLength > 0))
        {
//            std::cout << "finished" << std::endl;
            attacking = false;
            pullPunch = false;
            punchLength = 0;

            // Toista iskua varten
            if (firstAttack == false)
            {
                firstAttack = true;
            }
            else
            {
                firstAttack = false;
            }
            punchTime.restart();
        }
    }
    punching.setSize(sf::Vector2f(punchLength,sizeW/5));


    if (firstAttack == true and attacking == false and punchTime.getElapsedTime().asSeconds() > 0.5)
    {
        firstAttack = false;
    }
}


void Character::drawCharacter(sf::RenderWindow* Window, sf::Time theClock)
{
    Window->draw(shadow);
    hitBox.setPosition(posX, posY - airPos);
    Window->draw(hitBox);

    if (lookingLeft == false)
    {
        if (firstAttack == false)
        {
            punching.setPosition(posX + sizeW, posY + sizeH/7);
        }
        else if (firstAttack == true)
        {
            punching.setPosition(posX + sizeW, posY + sizeH/7 * 3);
        }
    }
    else
    {
        if (firstAttack == false)
        {
            punching.setPosition(posX, posY + sizeH/7);
        }
        else if (firstAttack == true)
        {
            punching.setPosition(posX, posY + sizeH/7 * 3);
        }
    }

//    punching.setPosition(200, 300);
    Window->draw(punching);
}

void Character::drawLife(sf::RenderWindow* Window, float movex, float screenH)
{
    calculateLife(screenH);

    if (type < 2)
    {
        if (movex > 0)
        {
            maxHealth.move(movex, 0);
            currentHealth.move(movex, 0);
        }

        Window->draw(maxHealth);
        Window->draw(currentHealth);
    }
    else
    {
        if (showHealthPos > 0)
        {
            maxHealth.setPosition(showHealthPos, screenH/20 * 2);
            currentHealth.setPosition(showHealthPos, screenH/20 * 2);

            Window->draw(maxHealth);
            Window->draw(currentHealth);
        }
        /*currentHealth.setPosition(20, screenH/20);
        maxHealth.setPosition(20,screenH/20);*/
    }
}

void Character::calculateLife(float screenH)
{
    float multiplier = health / stamina;
    currentHealth.setSize(sf::Vector2f(barSize * multiplier ,screenH/30));
}

void Character::resetLifePos(float screenW, float screenH)
{
    if (type == 0)
    {
        currentHealth.setPosition(20, screenH/20);
        maxHealth.setPosition(20,screenH/20);
    }
    else if (type == 1)
    {
        currentHealth.setPosition(screenW/2 + 10, screenH/20);
        maxHealth.setPosition(screenW/2 + 10,screenH/20);
    }
}

bool Character::checkDeath()
{
    if (health <= 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Character::getType()
{
    return type;
}

float Character::getHeight()
{
    return sizeH;
}

float Character::getWidth()
{
    return sizeW;
}

float Character::getPosX()
{
    return posX;
}

float Character::getPosY()
{
    return posY;
}

void Character::setPosition(float positioX, float positioY)
{
    posX = positioX;
    posY = positioY;
    shadow.setPosition(posX - sizeW/4,posY + sizeH - sizeW/4);
}

void Character::setActive()
{
    active = true;
}

bool Character::getActive()
{
    return active;
}

bool Character::checkItemProximity(Object* item)
{
    if (jumping == false)
    {
        if (posX + sizeW >= item->getPosX() and posX <= item->getPosX() + item->getWidth())
        {
            if (posY + sizeH > item->getPosY() + item->getHeight() - 20 and posY + sizeH < item->getPosY() + item->getHeight() + 20)
            {
                // Parannustavara
                if (item->getHealing() > 0)
                {
                    health = health + item->getHealing();
                    if (health > stamina)
                    {
                        health = stamina;
                    }
                }
                else if(item->getHealing() < 0)
                {
                    return false;
                }


                return true;
            }
        }
    }

    return false;
}

void Character::checkEnemyProximity(Character* enemy)
{
    float landposy = sizeH + posY;
    float enemlandposy = enemy->getHeight() + enemy->getPosY();

    if (landposy >= enemlandposy - 20 and landposy <= enemlandposy + 20 and enemy->isJumping() == false)
    {
        float grabRightEdge = 0;
        float grabLeftEdge = 0;
        if (lookingLeft == true)
        {
            grabRightEdge = posX + sizeW/2;
            grabLeftEdge = posX;
        }
        else
        {
            grabRightEdge = posX + sizeW;
            grabLeftEdge = posX + sizeW - sizeW/2;
        }
        float enemyGrabCenter = enemy->getPosX() + enemy->getWidth()/2;
        float enemyGrabDeviation = enemy->getWidth()/4;

        // Otetaan kiinni
        if (grabRightEdge >= enemyGrabCenter - enemyGrabDeviation and grabLeftEdge <= enemyGrabCenter + enemyGrabDeviation)
        {
            grappling = true;
            enemy->setGrabbled(true);
            grabTarget = enemy;
//            std::cout << "grab" << std::endl;
        }
        /*
        grappling = true;
        enemy->setGrabbled();
        */
    }
}

void Character::startFlying(SUUNTA direction, float power, float air)
{
    airSpeed = air;
    jumping = true;
    flying = true;

    /*float varaH = sizeH;
    sizeH = sizeW;
    sizeW = varaH;
    hitBox = sf::RectangleShape(sf::Vector2f(sizeW, sizeH));*/

    flyingSpeed = power;
    flyDir = direction;
}

void Character::setGrabbled(bool grab)
{
    grappled = grab;
}

bool Character::isJumping()
{
    return jumping;
}

bool Character::isAttacking()
{
    return attacking;
}

bool Character::isFlying()
{
    return flying;
}

void Character::setTextSprite(std::string path, std::string suffix, int frames, ANIMATIONTYPE type)
{
    for (int i = 0; i < frames; ++i)
    {
        std::string number = "";
        std::stringstream ss;
        ss << i;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture texture;
        if (!texture.loadFromFile(whole))
        {
            std::cout << "Ei loydy " << whole << std::endl;
        }

        if (type == RUN)
        {
            runText.push_back(texture);
        }
    }

    for (unsigned int i = 0; i < frames; ++i)
    {
        if (type == RUN)
        {
            runText.at(i).setSmooth(true);
            sf::Sprite run;
            //run.setOrigin(250,250);
            run.setTexture(runText.at(i));
            //run.setScale(0.4,0.4);
            SpriteDetail(run);
            runSprite.push_back(run);
        }
    }
}

void Character::SpriteDetail(sf::Sprite& sprite)
{
    sprite.setOrigin(250,250);
    sprite.setScale(0.4,0.4);
}

void Character::setTexturesSprites(std::string path, std::string suffix, int frames, std::vector<sf::Texture>& textures, std::vector<sf::Sprite>& sprites, float scale, float xOrigin, float yOrigin)
{
    for (int a = 0; a < frames; ++a)
    {
        std::string number = "";
        std::stringstream ss;
        ss << a;
        number = ss.str();
        std::string whole = path + number + suffix;

        sf::Texture text;
        if(!text.loadFromFile(whole))
        {
            std::cout << "Ei loydy " << whole << std::endl;
        }
        textures.push_back(text);
    }

    for (unsigned int i = 0; i < textures.size(); ++i)
    {
        textures.at(i).setSmooth(true);
        sf::Sprite sprite;
        sprite.setOrigin(xOrigin , yOrigin);
        sprite.setTexture(textures.at(i));
        sprite.setScale(scale , scale);
        sprites.push_back(sprite);
    }
}

void Character::setHitColor(float screenW, float screenH)
{
    // Player 1
    if (type == 0)
    {
        hitBox.setFillColor(sf::Color(0,0,255,128));
        punching.setFillColor(sf::Color(0,0,255,128));

        currentHealth.setPosition(20, screenH/20);
        maxHealth.setPosition(20,screenH/20);
    }
    // Player 2
    else if(type == 1)
    {
        hitBox.setFillColor(sf::Color(0,255,0,128));
        punching.setFillColor(sf::Color(0,255,0,128));

        currentHealth.setPosition(screenW/2 + 10, screenH/20);
        maxHealth.setPosition(screenW/2 + 10,screenH/20);
    }
    // Boss
    else if(type == 2)
    {
        hitBox.setFillColor(sf::Color(127,0,127,128));
        punching.setFillColor(sf::Color(127,0,127,128));
    }
    // enemy
    else
    {
        hitBox.setFillColor(sf::Color(255,0,0,128));
        punching.setFillColor(sf::Color(255,0,0,128));
    }
    hitBox.setPosition(posX,posY);
    punching.setPosition(posX, posY);

    //shadow.setFillColor(sf::Color::Black);
    shadow.setFillColor(sf::Color(10,10,10,100));
}
