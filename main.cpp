#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include "Engine.h"

int main()
{

    Engine* Enginsan = 0;
    Enginsan = new Engine();

    Enginsan->run();

    delete Enginsan;
    Enginsan = 0;

    return 0;
}
