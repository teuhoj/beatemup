#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
//#include <strstream>
#include <vector>

#include "Player.h"

#include "Character.h"
#include "BillyChing.h"
#include "KaiserChef.h"
#include "Thug.h"

#include "Object.h"
#include "Wonton.h"
#include "Destructible.h"
#include "ThrowToEnvironment.h"

#include "Includes.h"

#ifndef ENGINE_H
#define ENGINE_H

enum EVENTTYPE {LOCK, SPAWNTHUG, LEVEL_END, SPAWNWONTON, SPAWNDESTOBJ};

struct EVENT
{
    float xKoord;
    float yKoord;
    EVENTTYPE tyyppi;
    DESTSPAWN contains;
    LOOKS appearance;
};

struct DANGER
{
    sf::RectangleShape zone;
    float damage;
    float stagePosY;
    float healthPos;
};

class Engine
{
    public:
        Engine();
        virtual ~Engine();

        void run();

        void updateGame(std::vector<DANGER> dangerAreas, sf::Time theClock);

        void selectCharScreen();

    protected:
    private:

        sf::RenderWindow* App;

        std::vector <std::string> levelNames;


        Player* Player1con;


        BillyChing* Player1;
        BillyChing* Player2;
        Thug* Enemy;
        Character* Boss;


        // Resurssit vihollisille
        std::vector <Thug*> Thugs;

        // Tavararesurssit
        // Wonton
        std::vector <Wonton*> Wontons;

        // Tuhottavat esineet
        std::vector <Destructible*> Destructibles;

        sf::Sound whoosh;
        sf::Sound light_hit;
        bool playhit;

        sf::SoundBuffer lBuffer;
        sf::Sound lightning;

        int framesPerSecond;
        int frames;
        sf::Time oneSecond;
        sf::Text FPS;
        sf::Text remainingTime;
        int timeRemain;

        sf::Font viking;

        float screenW;
        float screenH;

        float screenLimit;

        float gravity;
        float airFriction;

        bool p1attack;
        bool p1jump;
        bool p2attack;
        bool p2jump;

        sf::Texture backGround;
        sf::Sprite BACKG;

        sf::Music bgMusic;

        sf::View view;

        float leftEdge;
//        sf::View view2p;

        std::vector <EVENT> tapahtumat;

        bool locking;
        bool endingLevel;
        bool levelCleared;

        float p1runner;
        bool p1running;
        int p1runRight;
        float p2runner;
        bool p2running;
        int p2runRight;
};

#endif // ENGINE_H
