#ifndef KAISERCHEF_H
#define KAISERCHEF_H
#include "Character.h"

class KaiserChef : public Character
{
    public:
        KaiserChef(int align, float setX, float setY, float hp, float screenH, float screenW);
        KaiserChef(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW);

        virtual ~KaiserChef();

        void drawCharacter(sf::RenderWindow* Window, sf::Time theClock);

        bool dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos);

        void attack();
        void throwTarget();

        void updatePunching(float graviy, float timeStuff);

    protected:
    private:

        sf::RectangleShape testPunch;

        bool drawSteam;
        int steamPos;
        float steamTime;
        int steamFrames;
        std::vector<sf::Texture> steamText;
        std::vector<sf::Sprite> steamSprite;
};

#endif // KAISERCHEF_H
