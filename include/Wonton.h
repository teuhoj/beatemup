#ifndef WONTON_H
#define WONTON_H

#include "Object.h"

// Basic Healing item
class Wonton : public Object
{
    public:
        Wonton(float xKoord, float yKoord, int jarj);
        virtual ~Wonton();

        void drawObject(sf::RenderWindow* Window);

    protected:
    private:
};

#endif // WONTON_H
