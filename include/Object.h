#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#ifndef OBJECT_H
#define OBJECT_H

#include <Includes.h>

enum ITEMTYPE{HEALING,WEAPON,FIREARM,SCORE,BREAKABLE};

class Object
{
    public:
        // PIIRTOJARJESTYS!!!
        Object(float xKoord, float yKoord, int jarj);
        virtual ~Object();

        void drawObject(sf::RenderWindow* Window);

        float getHealing();
        int getType();

        float getPosX();
        float getPosY();
        float getHeight();
        float getWidth();

        void setPosition(float positX, float positY);

        void jumpObject(float amount);
        void updateObject(float gravity, float timeStuff);

        float amountHealed;

    protected:

        float posX;
        float posY;

        float width;
        float height;

        ITEMTYPE type;

        sf::RectangleShape hitbox;
        sf::CircleShape shadow;

        float airSpeed;
        float airPos;
        bool jumping;

        int ali;

        sf::Texture tekstuuri;
        sf::Sprite kuva;

    private:
};

#endif // OBJECT_H
