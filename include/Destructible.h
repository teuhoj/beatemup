#ifndef DESTRUCTIBLE_H
#define DESTRUCTIBLE_H

#include "Object.h"



class Destructible : public Object
{
    public:
        // Siis sijainti, piirtonumero, ulkomuoto ja kesto
        Destructible(float xKoord, float yKoord, int jarj, LOOKS outside, DESTSPAWN sisalto);
        virtual ~Destructible();

        void takeDamage(sf::RectangleShape danger, float damage, float hitOriginY);
        DESTSPAWN getContents();

        float getHealth();

        void drawObject(sf::RenderWindow* Window);

    protected:
    private:
        LOOKS shell;
        DESTSPAWN contains;
        float health;
};

#endif // DESTRUCTIBLE_H
