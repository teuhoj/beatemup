#ifndef THUG_H
#define THUG_H
#include <vector>

#include "Character.h"

class Thug : public Character
{
    public:
        Thug(int align, float setX, float setY, float hp, float screenH, float screenW);
        Thug(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW);

        virtual ~Thug();

        void act(float limitPosY, float limitMaxY, float limitPosX, float limitMaxX, float timeMod, float player1posY, float player1posX);

        void drawCharacter(sf::RenderWindow* Window, sf::Time theClock);

    protected:
    private:
        sf::Clock behaviourSelect;
        float sinceLastBehaviour;
        BEHAVIOUR currentBehaviour;
        sf::Time inCurrentBehaviour;
};

#endif // THUG_H
