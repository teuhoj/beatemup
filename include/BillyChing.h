#ifndef BILLYCHING_H
#define BILLYCHING_H
#include "Character.h"

class BillyChing : public Character
{
    public:
        BillyChing(int align, float setX, float setY, float hp, float screenH, float screenW);
        BillyChing(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW);

        virtual ~BillyChing();

        void drawCharacter(sf::RenderWindow* Window, sf::Time theClock);

        bool dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos);

        void attack();
        void throwTarget();

        void updatePunching(float graviy, float timeStuff);

    protected:
    private:

        sf::RectangleShape testPunch;
};

#endif // BILLYCHING_H
