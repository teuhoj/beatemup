#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#ifndef CHARACTER_H
#define CHARACTER_H

#include <Object.h>
#include <Wonton.h>
#include <Includes.h>

enum SUUNTA {VASEN, OIKEA, YLOS, ALAS, EIOLE};
enum BEHAVIOUR {LAHESTY, KIERRA, ODOTA, last};

enum ANIMATIONTYPE {JUMP, PUNCH1,PUNCH2, RUN, STANCE, WALK};

class Character
{
    public:
//        Character();
        Character(int align, float setX, float setY, float hp, float screenH, float screenW);
        Character(float height, float width, int align, float setX, float setY, float hp, float screenH, float screenW);

        virtual ~Character();

        void resetCharacter();

        void jumpCharacter(float liftOff);
        void moveCharacter(SUUNTA direc, float amount, float limitPosY, float limitMaxY, float limitPosX, float limitMaxX, bool keepRunning);
        void attack();
        void throwTarget();

        bool dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos);

        void takeHit(sf::RectangleShape danger, float damage, float hitOriginY, float healthPos);

        void updateCharacter(float gravity, float timeStuff, float airFriction);
        void updatePunching(float gravity, float timeStuff);

        void drawCharacter(sf::RenderWindow* Window, sf::Time theClock);
        void drawLife(sf::RenderWindow* Window, float movex, float screenH);

        void calculateLife(float screenH);

        void resetLifePos(float screenW, float screenH);

        bool checkDeath();

        int getType();
        float getHeight();
        float getWidth();

        float getPosY();
        float getPosX();

        void setPosition(float positioX, float positioY);

        void setActive();
        bool getActive();

        bool checkItemProximity(Object* item);

        void checkEnemyProximity(Character* enemy);
        void startFlying(SUUNTA direction, float power, float air);

        void setGrabbled(bool grab);

        bool isJumping();
        bool isAttacking();
        bool isFlying();

        void setTextSprite(std::string path, std::string suffix, int frames, ANIMATIONTYPE type);
        void SpriteDetail (sf::Sprite& sprite);

        void setTexturesSprites(std::string path, std::string suffix, int frames, std::vector<sf::Texture>& textures, std::vector<sf::Sprite>& sprites, float scale, float xOrigin, float yOrigin);

        int standFrames;
        int walkFrames;
        int punch1Frames;
        int jumpFrames;
        int runFrames;

        int standPos;
        float standTime;

        int punchPos;
        float punchAika;

        int walkPos;
        float walkAika;

        int jumpPos;

        int runPos;
        float runAika;

    protected:
        float sizeW;
        float sizeH;

        float posX;
        float posY;

        float airPos;
        float airSpeed;
        float startAirSpeed;

        float health;
        float stamina;
        float barSize;
        int lives;

        sf::RectangleShape hitBox;
        // Testi� varten
        sf::RectangleShape punching;
        float maxPunchLength;
        float punchLength;
        bool pullPunch;

        sf::RectangleShape currentHealth;
        sf::RectangleShape maxHealth;
        sf::CircleShape shadow;

        int type;

        bool jumping;
        bool attacking;
        bool walking;

        bool firstAttack;

        void setHitColor(float screenW, float screenH);

        // Sounds
        sf::SoundBuffer swingBuffer;
        sf::Sound swing;
        sf::SoundBuffer lightHitBuffer;
        sf::Sound lightHit;

        float flip;

        std::vector<sf::Texture> standingText;
        std::vector<sf::Sprite> standingSprite;

        std::vector<sf::Texture> walkText;
        std::vector<sf::Sprite> walkSprite;

        std::vector<sf::Texture> punch1Text;
        std::vector<sf::Sprite> punch1Sprite;

        int punch2Frames;
        std::vector<sf::Texture> punch2Text;
        std::vector<sf::Sprite> punch2Sprite;

        std::vector<sf::Texture> jumpText;
        std::vector<sf::Sprite> jumpSprite;

        std::vector<sf::Texture> runText;
        std::vector<sf::Sprite> runSprite;

        sf::Clock punchTime;

        int comboCounter;
        sf::Clock comboTime;
        float comboTimeSeconds;

        bool lookingLeft;

        bool hitting;

        float showHealthPos;

        bool active;
        bool running;

        bool grappling;
        bool grappled;

        sf::Clock grappleClock;
        float totalGrappleTime;

        SUUNTA throwDir;
        Character* grabTarget;
        bool flying;
        float flyingSpeed;
        float flyingPower;
        SUUNTA flyDir;
        float flyingPos;
    private:
};

#endif // CHARACTER_H
