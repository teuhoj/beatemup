#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "BillyChing.h"
#include "KaiserChef.h"

enum SELECTED {BILLY, KAISER, NOTSELECTED};

class Player
{
    public:
        Player();
        virtual ~Player();

        void selectBilly(float screenH, float screenW, int type);
        void selectKaiser(float screenH, float screenW, int type);

        void setCharacterPosition(float xKoord, float yKoord);
        void resetLifePosition(float screenH, float screenW);
        void resetCharacter();

        // SUUNTA CHARACTERILTA
        void moveCharacter(SUUNTA direc, float amount, float limitPosY, float limitMaxY, float limitPosX, float limitMaxX, bool keepRunning);
        void jumpCharacter(float liftOff);
        void attackCharacter();

        void checkEnemyProximity(Character* enemy);
        bool checkItemProximity(Object* item);

        bool isCharacterAttacking();
        bool isCharacterJumping();

        bool dangerZone(sf::RectangleShape& danger, float& damage, float& playerPosY, float& healthPos);

        void updateCharacter(float gravity, float timeStuff, float airFriction);
        void updatePunching(float gravity, float timeStuff);

        void drawCurrentCharacter(sf::RenderWindow* Window, sf::Time theClock);
        void drawLife(sf::RenderWindow* Window, float movex, float screenH);

        Character* returnCurrentCharacter();

        float getPosY();
        float getPosX();

        float getHeight();
        float getWidth();

    protected:
    private:
        SELECTED myChoice;

        BillyChing* Billy;
        KaiserChef* Kaiser;
        Character* Chara;
};

#endif // PLAYER_H
